export const ROUTES = [
  '/',
  '/forms',
  '/forms/controls-and-validations',
  '/forms/extended-controls',
  '/forms/sample-layouts',
  '/forms/material-forms',
  '/forms/wizard',
  '/tables',
  '/tables/regular'
];
