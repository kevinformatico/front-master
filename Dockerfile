FROM pgstdvbap01.cl.bsch:8480/runtime/chile-runtime-nginx:1.17-alpine

COPY ./dist/FrontEndHanshakeManager /usr/share/nginx/html

EXPOSE 8080
