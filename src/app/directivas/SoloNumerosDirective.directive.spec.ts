import { SoloNumerosDirective } from './soloNumeros.directive';
import { ElementRef } from '@angular/core';


describe('SoloNumerosDirective', () => {
    it('should create an instance', () => {
        let a: ElementRef = {nativeElement: {value: '949548998'}};
      const directive = new SoloNumerosDirective(a);
      expect(directive).toBeTruthy();
    });
  });
  