import { Directive, ElementRef, HostListener, OnInit, Input } from '@angular/core';
@Directive({
  selector: '[appSoloNumeros]'
})
export class SoloNumerosDirective {



  private regex: RegExp = /^[0-9]+(\.[0-9]*){0,1}$/g ;
  private el: HTMLInputElement;

  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home'];

  constructor(private elementRef: ElementRef) {
    this.el = elementRef.nativeElement;
  }


  @HostListener('keydown', ['$event'])
  onkeydown(event: KeyboardEvent) {
    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    const current = this.el.value;
    const next = current.concat(event.key);

    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }
    
 
  


}
