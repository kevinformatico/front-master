import { Directive, ElementRef, HostListener } from '@angular/core';

@Directive({
  selector: '[appSoloLetrasNumeros]'
})
export class SoloLetrasNumerosDirective {
  private regex: RegExp = /^[a-zA-Z0-9]*$/;

  private specialKeys: Array<string> = ['Backspace', 'Tab', 'End', 'Home'];

  constructor(private el: ElementRef) {}
  @HostListener('keydown', ['$event'])
  onKeyDown(event: KeyboardEvent) {

    if (this.specialKeys.indexOf(event.key) !== -1) {
      return;
    }
    const current = this.el.nativeElement.value;
    const next = current.concat(event.key);
    if (next && !String(next).match(this.regex)) {
      event.preventDefault();
    }
  }
}