
import { Injectable } from '@angular/core';

declare const propertiesGlobal;

@Injectable({
    providedIn: 'root'
})

export class PropertiesService {
    public properties: {
        production: boolean,
        name: string,
        URI_DATAPOWER: string,
        URI_LATINIA: string
    };

    constructor() {
        this.properties = propertiesGlobal;
    }
}