import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../interface/user.interface';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { ContextoService } from './contexto.service';
import { PropertiesService } from '../services/propertiesService';

// import { GLOBAL } from './global';


@Injectable({
  providedIn: 'root'
})
export class LoginService {



  private urlAccounts: string;
  private endpoint: string;

  constructor(public http: HttpClient, private cxtService: ContextoService, private propertiesSrv:PropertiesService) {
    this.endpoint = this.propertiesSrv.properties.URI_DATAPOWER;
  }

  private addElements(user: User): User {
    return { ...user, canal: '91', app: 'ARH' };
  }

  public loginAuth(user: User): Observable<any> {
    const json = JSON.stringify(user);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.endpoint+'login', json, { headers: headers });
  }


  public logOut(): Observable<any> {
    const json = '{ "RutUsuario":"' + this.cxtService.login.rutUsuario + '"}';
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.endpoint + 'logout', '{ "RutUsuario":"' + this.cxtService.login.rutUsuario + '"}', { headers: headers });
  }



}


