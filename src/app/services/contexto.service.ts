import { Injectable } from '@angular/core';
import {Login } from './model/login';



@Injectable({
  providedIn: 'root'
})
export class ContextoService {

  private _login: Login;
  private _token = '';


  constructor() {
    //this.initContexto();
  }




  get login(): Login { return this._login; }
  set login(login: Login) { this._login = login; }



  get token(): string { return this._token; }
  set token(value: string) { this._token = value; }
/*
guardarLogin(login: Login) {
    this._login = login;
}/*


  /**
   * Realiza la limpieza de la informacion guardada en contexto.
   * La informacion considerada en el reset es aquella ingresada por el usuario
   * en el sitio, como tambien las respuestas recibidas por los diferentes
   * servicios backend utilizados.
   */
  reset() {
    this._token = '';
    this._login = null;
   // this.initContexto();
  }
}
