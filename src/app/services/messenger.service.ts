import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { ContextoService } from './contexto.service';
import { PropertiesService } from './propertiesService';

// import { GLOBAL } from './global';


@Injectable({
  providedIn: 'root'
})
export class MessengerService {

  private endpoint: string;

  constructor(public http: HttpClient, private cxtService: ContextoService, private propertiesSrv:PropertiesService) {
    this.endpoint = this.propertiesSrv.properties.URI_LATINIA;
  }



  public sendMail(subject:string,mailTo:string,messenger:string): Observable<any> {
    var xmlString = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:adap="http://adaptors.j2ee.limsp.latinia.com">' +
    ' <soapenv:Header/>' +
    ' <soapenv:Body>' +
    ' <adap:putMessage soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/">' +
    '     <string xsi:type="xsd:string">' +
    '<![CDATA[ <message id=' + "'um1o5axykzckdev8cvs584l9' ts='1464028204'>" +
    '<head>' +
    "<type ref='email'>" +
    "<format>email</format>" +
    " <mroute>out</mroute>" +
    " </type>" +
    " <info>" +
    "<loginEnterprise>SCHCL</loginEnterprise>" +
    " <refContract>APPPSN01</refContract>" +
    " <customer Keyname='refUser' KeyValue=''/>" +
    " <deliveryChannels max='1'>email</deliveryChannels>" +
    " <subject>'" + subject + "'</subject>" +
    "</info>" +
    "<address-list>" +
    " <address type='from' class='email' ref='mensajeria@santander.cl'/>" +
    " <address type='to' class='email' ref='" + mailTo + "'/>" +
    "</address-list>" +
    " </head>" +
    " <body>" +
    "<content refTemplate='BAU'></content>" +
    "<templateParams ref='#default'>" +
    "<param name='mensaje'>'" + messenger+ "'</param>" +
    "</templateParams>" +
    "</body>" +
    " </message>" +
    "]]>" +
    "</string>" +
    " </adap:putMessage>" +
    "</soapenv:Body>" +
    "</soapenv:Envelope>";
    let parser = new DOMParser();
    const data = parser.parseFromString(xmlString, "application/xml");
    const headers = new HttpHeaders().set('Content-Type', 'application/xml');
    return this.http.post(this.endpoint, data, { headers: headers });
  }


  




}


