import { Component, ViewChild, ViewEncapsulation, Inject, PLATFORM_ID, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { SelectionModel } from '@angular/cdk/collections';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ConsultaService } from './services/consulta.services';
import { LoadScreenService } from '../loading/load-screen.service';
import { LoadingInfo } from '../loading/load-screen.component';
import { BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import { ErrorService } from '../services/error.service';
import { ContextoService } from '../services/contexto.service';

const COMMA = 188;
const ENTER = 13;

export interface RowData {
  solicitante: string;
  nameApp: string;
  fecha: string;
  version: string;
  build: string;
  id: string;
  codApp: string;
  canal: string;
  mensaje: string;
  accion: string;
  os: string;
  aprobadores: any;
}

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './dashboard.component.html',
  styleUrls: [
    './styles/dashboard.scss'
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})

export class DashboardPageComponent implements OnInit {
  charts: any = {};
  variablesShow: any;
  showDialog: boolean = false;

  tableDisplayedColumns: string[] = ['select', 'solicitante', 'nameApp', 'fecha', 'version', 'build', 'codApp', 'canal', 'mensaje', 'os', 'aprobadores'];
  tableDataSource = undefined;
  baseUrl = '';
  isBrowser: boolean;
  showTooltip: boolean = false;
  dato3: string;
  dato1: string;
  dato2: string;
  status1: string;
  status2: string;
  status3: string;
  flagSolicitante: boolean = false;
  flagAprobador: boolean = false;
  tituloComment: string = '';
  detalleComment: string = '';
  tooldata = [];
  cantAprob: string;
  displayDialogEliminar: boolean = false;
  validateResAprobador2: boolean = false;

  // Table 2 - local filters
  displayedColumnsTable2: string[] = ['select', 'solicitante', 'nameApp', 'fecha', 'version', 'build', 'codApp', 'canal', 'mensaje', 'os', 'aprobadores'];
  dataSourceTable2: MatTableDataSource<RowData>;
  dataSource: MatTableDataSource<RowData>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('paginatorTable2') paginatorTable2: MatPaginator;
  @ViewChild('sortTable2') sortTable2: MatSort;
  selection = new SelectionModel<RowData>(true, []);
  rutUserService: string = '';
  visualTable : boolean = false;
  // Data from the resolver
  originalData = [];
  listSolicitud = [];
  valuestring = '';
  loadingStatus: BehaviorSubject<LoadingInfo>;
  displayComent = false;
  public solicitudes: any;
  public solicitud: any;
  // Tags interests
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes = [COMMA, ENTER];
  interests = [];
  progress = '0';
  colorBar = '';
  tituloAlerta: string = '';
  detalleAlerta: string = '';
  validateResSolcitante: boolean = false;
  varExito: boolean = false;
  errorSelec: boolean = false;
  // Filters for the smart table
  filtersForm: FormGroup;
  filtersVisible = true;
  toggleFiltersLabel = 'Hide filters';
  rutUserAprob: string;
  validateResAprobador: boolean = false;
  flagNull: boolean = false;
  

  ngOnInit() {
    if(this.listSolicitud.length !== 0){
      this.visualTable = true;
      this.dataSourceTable2.sort = this.sort;
      this.dataSourceTable2.paginator = this.paginatorTable2;
    }else {
      this.flagNull = true;
    }

  }


  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceTable2.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceTable2.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: RowData): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.id + 1}`;
  }

  applyFilterTable2(filterValue: string) {
    this.dataSourceTable2.filter = filterValue.trim().toLowerCase();

    if (this.dataSourceTable2.paginator) {
      this.dataSourceTable2.paginator.firstPage();
    }
  }

  constructor(
    private loadingSrv: LoadScreenService,
    public dialog: MatDialog,
    private _router: Router,
    private srvConsulta: ConsultaService,
    private ctxSrv: ContextoService,
    private errSrv: ErrorService,

    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.loadingStatus = new BehaviorSubject({ status: false });
    this.loadingSrv.setHttpStatus(false);
    this.rutUserService = this.ctxSrv.login.rutUsuario;
    this.rutUserAprob = this.ctxSrv.login.Profile;
    this.cargarLista();
    this.dataSourceTable2 = new MatTableDataSource(this.listSolicitud);
  }




  cargarLista() {
    if (this.rutUserAprob === 'Aprobador') {
      this.srvConsulta.getSolicitudAprobador(this.rutUserService).subscribe(
        result => {
          const flagData = result.DATA.SearchData_Response.OUTPUT.hits.total.value;
          if (flagData === 0) {
            this.flagNull = true;
            return;
          }
          this.solicitudes = result.DATA.SearchData_Response.OUTPUT.hits.hits;
          const ELEMENT_DATA: RowData[] = this.solicitudes;
          this.dataSource = new MatTableDataSource(ELEMENT_DATA);
          let i = 0;
          while (i < this.solicitudes.length) {
            for (let index = 0; index < result.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.aprobadores.length; index++) {
              if(result.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.aprobadores[index].status === 'en proceso' && result.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.aprobadores[index].rut === this.rutUserService){
                this.listSolicitud.push(result.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source);
              }
              
            }
            i++;
          }
            this.flagAprobador = true;
            this.dataSourceTable2 = new MatTableDataSource(this.listSolicitud);
        },
        error => {
          this.errSrv.irPaginaError();
        }
      );
    }
    if (this.rutUserAprob === 'Solicitante') {
      this.srvConsulta.getSolicitudes(this.rutUserService).subscribe(
        result => {
          const flagData = result.DATA.SearchData_Response.OUTPUT.hits.total.value;
          this.solicitudes = result.DATA.SearchData_Response.OUTPUT.hits.hits;
          if (flagData === 0) {
            this.flagNull = true;
            return;
          }
          const ELEMENT_DATA: RowData[] = this.solicitudes;
          this.dataSource = new MatTableDataSource(ELEMENT_DATA);
          let i = 0;
          while (i < this.solicitudes.length) {
            if (result.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.type === 'solicitudes') {
              this.listSolicitud.push(result.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source);
            }
            i++;
          }
            this.flagSolicitante = true;
            this.dataSourceTable2 = new MatTableDataSource(this.listSolicitud);
        },
        error => {
          this.errSrv.irPaginaError();
        }
      );
    }
  }

  updateProgress(p1: any, p2?: any, p3?: any): boolean {
    let data;

    if (p2 && p2.length !== 0) {
      data = [p1, p2];
    }
    if (p3 && p3.length !== 0) {
      data = [p1, p2, p3];
    } if (!p3 && !p2) {
      data = [p1];
    }

    const aprob = data.filter(function (data) {
      return data === 'aprobado';
    });


    const rech = data.filter(function (data) {
      return data === 'rechazado';
    });

    if (rech.length >= 1) {
      this.colorBar = '2';
      this.valuestring = 'Rechazada';
      this.progress = 'Solicitud';
      return true;
    }
    else {
      if (aprob.length === 3) {
        this.colorBar = '1';
        this.valuestring = 'aprobado';
        //this.progress = (Math.floor((aprob.length / (data.length)) * 100)).toString();
        this.progress = aprob.length + '/' + data.length;
        return true;
      }
      if (data.length === aprob.length) {
        this.colorBar = '1';
        this.valuestring = 'aprobado';
        //this.progress = (Math.floor((aprob.length / (data.length)) * 100)).toString();
        this.progress = aprob.length + '/' + data.length;
      }
      if (aprob.length >= 1) {
        this.colorBar = '3';
        this.valuestring = 'aprobado';
        //this.progress = (Math.floor((aprob.length / (data.length)) * 100)).toString();
        this.progress = aprob.length + '/' + data.length;

        return true;
      } else {
        this.colorBar = '3';
        this.valuestring = 'En proceso';
        //this.progress = (Math.floor((data.length / (data.length)) * 100)).toString();
        this.progress = aprob.length + '/' + data.length;

        return true;
      }

    }

  }
  onclicktooltip(dato) {

    this.tooldata = dato;
    this.showTooltip = true;
  }

  ocultarAlerta() {
    this.tooldata = [];
    this.showTooltip = false;
  }


  public aprobar(selection): void {
    this.ocultarAlertaAprob();
    this.loadingStatus.next({ status: true, titulo: 'Actualizando tu selección.' });
    this.loadingSrv.setHttpStatus(true, 'Actualizando tu selección.');

    for (let s of selection.selected) {
      let dato = s['aprobadores'].length;
      let solicitud = this.solicitudes.filter(function (data) {
        return data._source.nameApp === s['nameApp'];
      });
      for (let i = 0; i < dato; i++) {
        if (s['aprobadores'][i].rut === this.rutUserService) {
          s['aprobadores'][i]['status'] = 'aprobado';
        }
      }
    }
    this.validateStatusSolicitud(selection);
  }

  public validateStatusSolicitud(selection): void {
    let aprobaciones = 0;
    let aprobadorOK = 0;
    for (let a of selection.selected) {
      let aprobadores = a['aprobadores'].length;
      let solicitud = this.solicitudes.filter(function (data) {
        return data._source.nameApp === a['nameApp'];
      });
      for (let b = 0; b < aprobadores; b++) {
        if (a['aprobadores'][b]['status'] === 'aprobado') {
          aprobaciones = aprobaciones + 1;
          if(a['aprobadores'][b]['rut'] === this.rutUserService){
            aprobadorOK++;
          }
        }
      }      
      if (aprobaciones === aprobadores) {
        a['type'] = 'appproductiva';
        this.srvConsulta.insertApp(a).subscribe(
          res => {
            if (res.DATA.InsertSolicitud_Response.INFO.Codigo === "00" || res.DATA.InsertData_Response.INFO.Codigo === "00") {
              a['type'] = 'solicitudes';
              a['status'] = 'INACTIVA';
              this.srvConsulta.updateStatusSolicitud(solicitud[0]._id, a['status']).subscribe(
                res => {
                  if (res.DATA.UpdateData_Response.INFO.Codigo === "16") {
                    this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Oops ha ocurrido un error al procesar tu solicitud.' });
                  }
                }
              );
            }else{
              this.loadingStatus.next({ status: false });
              this.loadingSrv.setHttpStatus(false);
              this.tituloAlerta = 'Error';
              this.detalleAlerta = 'ha ocurrido un error';
              this.errorSelec = true;
            }
           
          });
      } else {
       this.srvConsulta.updateSolicitud(solicitud[0]._id, a).subscribe(
          res => {
            if (res.DATA.UpdateData_Response.INFO.Codigo !== "00") {
              this.loadingStatus.next({ status: false });
              this.loadingSrv.setHttpStatus(false);
              this.tituloAlerta = 'Error';
              this.detalleAlerta = 'ha ocurrido un error';
              this.errorSelec = true;
            }
          });
      }
      if(selection.selected.length === aprobadorOK){
        this.ocultaLoadingAprobador();
      }
    }
      
    
  }

  rechazar(): void {
    this.tituloAlerta = 'Rechazo solicitud';
    this.detalleAlerta = 'Estas seguro de rechazar la selección.';
    this.validateResAprobador2 = true;

  }

  cancelar() {
    this.tituloComment = 'Cancelar solicitud';
    this.detalleComment = '¿Estas seguro de cancelar la solicitud?';
    this.displayDialogEliminar = true;
  }


  ocultaLoadingAprobador() {
    this.loadingStatus.next({ status: false });
    this.loadingSrv.setHttpStatus(false);
    this.tituloAlerta = "Exitosa";
    this.detalleAlerta = 'Se ha aprobado la solicitud de manera exitosa.';
    this.mostrarAlerta1();

  }

  ocultaLoading() {
    this.loadingStatus.next({ status: false });
    this.loadingSrv.setHttpStatus(false);
    this.tituloAlerta = "Se a Cancelado tu solicitud de manera Exitosa";
    this.mostrarAlerta1();

  }





  ocultarAlertaSol() {
    this.validateResSolcitante = false;
  }

  mostrarAlerta1() {

    this.varExito = true;
  }

  ocultarAlertaAprob2() {
    this.validateResAprobador2 = false;
  }

  ocultarAlerta1() {
    this.varExito = false;
    this._router.navigate(['/tables/listhistorico']);

  }


  eliminar(selection): void {
    this.displayDialogEliminar = false;
    this.loadingStatus.next({ status: false });
    this.loadingSrv.setHttpStatus(false);
    for (let s of selection.selected) {
      if (s['aprobadores'][0]['status'] === 'aprobado' && s['aprobadores'][1]['status'] === 'aprobado' && s['aprobadores'][2]['status'] === 'aprobado') {
        this.tituloAlerta = 'Error Selección';
        this.detalleAlerta = 'Debes seleccionar solicitudes que se encuentren en progreso';
        this.errorSelec = true;
        return;
      }
    }

    this.loadingStatus.next({ status: true, titulo: 'Cancelando tu solicitud seleccionada.' });
    this.loadingSrv.setHttpStatus(true, 'Cancelando tu solicitud seleccionada.');
    let solicitanteDelete = 0;
    for (let s of selection.selected) {
      s['status'] = 'INACTIVO';
      let solicitud = this.solicitudes.filter(function (data) {
        return data._source.nameApp === s['nameApp'];
      });

      this.srvConsulta.updateSolicitudCancelar(solicitud[0]._id, s).subscribe(res => {
        if (res.DATA.UpdateData_Response.INFO.Codigo === '16') {
          this.loadingStatus.next({ status: false });
          this.loadingSrv.setHttpStatus(false);
          this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Oops ha ocurrido un error al procesar tu solicitud.' });
        }else{
          solicitanteDelete++;
        }

      });
      if(selection.selected.length === solicitanteDelete){
        this.loadingStatus.next({ status: false });
      this.loadingSrv.setHttpStatus(false);
      this.tituloAlerta = "Exitosa";
      this.detalleAlerta = 'Se ha cancelado la solicitud de manera exitosa.';
      this.mostrarAlerta1();
      }
    }
   
  }

  callAlertaAprob() {
    this.tituloAlerta = "Aprobador Solicitud";
    this.detalleAlerta = "Estas seguro de aprobar la selección";
    this.validateResAprobador = true;
  }
  ocultarAlertaAprob() {
    this.validateResAprobador = false;
  }

  enviarComentario(selection) {
    this.validateResAprobador2 = false;
    this.loadingStatus.next({ status: true, titulo: 'Actualizando tu selección.' });
    this.loadingSrv.setHttpStatus(true, 'Actualizando tu selección.');
    let aprobaciones = 0;
    let aprobadorNOK = 0;
    for (let s of selection.selected) {
      aprobaciones++;      

      let dato = s['aprobadores'].length;
      let solicitud = this.solicitudes.filter(function (data) {
        return data._source.nameApp === s['nameApp'];
      });
      for (let i = 0; i < dato; i++) {
        if (s['aprobadores'][i].rut === this.rutUserService) {
          s['aprobadores'][i]['status'] = 'rechazado';
          aprobadorNOK++;
        }
      }
      this.srvConsulta.updateSolicitudRespAprob(solicitud[0]._id, s['aprobadores']).subscribe(
        res => {
          if (res.DATA.UpdateData_Response.INFO.Codigo == '16') {
            this.loadingStatus.next({ status: false });
            this.loadingSrv.setHttpStatus(false);
            this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Oops ha ocurrido un error al procesar tu solicitud.' });
          }
        }
      );
      if(selection.selected.length === aprobadorNOK){
        this.ocultaLoadingAprobador();
      }
    }



  }

  onClickAprobComment() {
    this.displayComent = true;
  }



  cerrarDialogoAprob() {
    this.displayComent = false;
  }

  cerrarDialogoSolici() {
    this.displayDialogEliminar = false;
  }

  cerrarError() {
    this.errorSelec = false;
  }
}
