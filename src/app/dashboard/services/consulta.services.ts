import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { ContextoService } from '../../services/contexto.service';
import { PropertiesService } from '../../services/propertiesService';

@Injectable({
    providedIn: 'root'
})
export class ConsultaService {
    headers = new HttpHeaders().set('Content-Type', 'application/json');


    private urlAccounts: string;

    constructor(public http: HttpClient, private ctxService: ContextoService, private propertiesSrv:PropertiesService) {
       this.urlAccounts = this.propertiesSrv.properties.URI_DATAPOWER+'api/';
    }
    /** Actualiza status Solicitud  aprobador*/
    updateSolicitudRespAprob(id:string, aprobadores): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada":{"doc":{ "aprobadores":' + JSON.stringify(aprobadores) + '}}}', { headers: headers });

    }

    updateStatusSolicitud(id, status): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada":{"doc":{ "status":' + JSON.stringify(status) + '}}}', { headers: headers });
    }
    updateSolicitud(id, solicitud): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada":{"doc":' + JSON.stringify(solicitud) + '}}', { headers: headers });
    }

    insertApp(app): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'insertType', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": ' + JSON.stringify(app) + '}', { headers: headers });
    }

    getSolicitudes(id: string): Observable<any> {
        return this.http.post(this.urlAccounts + 'search', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada":{"query":{"bool":{"must":[{"term":{"type": "solicitudes"}},{"term": {"id": "' + id.toLowerCase() + '"}},{"term":{"status":"activa"}}]}}}}', { headers: this.headers });
    }

    getSolicitudAprobador(id: any): Observable<any> {
        return this.http.post(this.urlAccounts + 'search', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada":{"query":{"bool":{"must":[{"term":{"type": "solicitudes"}},{"term":{"status":"activa"}},{"term": {"aprobadores.rut": "' + id + '"}},{"term": {"aprobadores.status": "proceso"}}]}}}}', { headers: this.headers });
    }


    /** Actualiza status Solicitud */
    updateSolicitudCancelar(id, solicitud): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": {"doc":' + JSON.stringify(solicitud) + '}}', { headers: headers });
    }


}
