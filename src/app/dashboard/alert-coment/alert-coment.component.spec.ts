import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertComentComponent } from './alert-coment.component';

describe('AlertComentComponent', () => {
    let component: AlertComentComponent;
    let fixture: ComponentFixture<AlertComentComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AlertComentComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AlertComentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});