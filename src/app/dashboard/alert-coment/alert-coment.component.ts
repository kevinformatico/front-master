import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';
import { MessengerService } from '../../services/messenger.service';


@Component({
  selector: 'app-alert-coment',
  templateUrl: './alert-coment.component.html',
  styleUrls: ['./alert-coment.component.scss']
})
export class AlertComentComponent implements OnInit {

  @Input() display = false;
  @Input() titulo: string;
  @Input() detalle: string;
  @Input() emailNoPermitido: string;

  @Output() callbackEnviar: EventEmitter<string> = new EventEmitter();
  @Output() callbackCerrar: EventEmitter<string> = new EventEmitter();


  formGroup: FormGroup;

  constructor(private sendM: MessengerService) {
    this.formGroup = new FormGroup({ email: new FormControl(null) });
  }
  validadores: ValidatorFn[] = [Validators.required, Validators.maxLength(60), Validators.minLength(20)];
  ngOnInit() {
    this.formGroup.setControl('email', new FormControl(null, this.validadores));
  }





  /**
   * Se ejecuta cueando se presiona el boton 'enviar' del formulario.
   */
  onClickEnviar() {
    const subject = 'aprobación solicitud';
    const mailTo = 'eliana.alexia@gmail.com'
    const frmCtrlComment = this.formGroup.get('email');




    this.callbackEnviar.emit(frmCtrlComment.value);

    this.sendM.sendMail(subject,mailTo,frmCtrlComment.value).subscribe(res => {
      if (res) {
        console.log(res);
      }
    }, err => {
      console.log(err);
    }
    );
  }

  /**
   * Se ejecua cuando se presiona el boton cerrar.
   */
  onClickCerrar() {
    const frmCtrlComment = this.formGroup.get('email');
    this.callbackCerrar.emit(frmCtrlComment.value);
  }

  get email() {
    return this.formGroup.get('email');
  }

}
