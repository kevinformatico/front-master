
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
//import { ChartsModule as ng2ChartsModule } from 'ng2-charts';

//import { ChartsDataService, Ng2ChartsResolver } from '../charts';
import { SharedModule } from '../shared';

import {  TableDataService } from '../tables';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';

import { DashboardPageComponent } from './dashboard.component';
import { DashboardResolver } from './dashboard.resolver';
import { ConsultaService } from './services/consulta.services';
import { MatTooltipModule, MatCardModule} from '@angular/material';
import { TooltipComponentComponent } from '../tooltip-component/tooltip-component.component';
import { CoreModule} from '../core/core.module';
import { AlertComentComponent } from './alert-coment/alert-coment.component';





export const DashboardRoutes = [
  {
    path: '',
    component: DashboardPageComponent,
    resolve: {
      data: DashboardResolver,
     // chart: Ng2ChartsResolver,
  //    table: ExtendedTablesResolver,
  //    tableData: SmartTablesResolver
    }
  },
];

@NgModule({
  declarations: [
    DashboardPageComponent,
    TooltipComponentComponent,
    AlertComentComponent
  ],
  imports: [
    RouterModule.forChild(DashboardRoutes),
    CommonModule,
    //ng2ChartsModule,
    SharedModule,
    MatTableModule,
    MatPaginatorModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    CoreModule,
    MatCardModule

  ],
  entryComponents: [
  ],
  providers: [
    DashboardResolver,
   // Ng2ChartsResolver,
   // ChartsDataService,
   // ExtendedTablesResolver,
    TableDataService,
   // SmartTablesResolver,
    ConsultaService
  ],
  exports:[
    MatSortModule,
    AlertComentComponent
  ]
})
export class DashboardModule { }
