import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';
import { PreloadAllModules, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TransferHttpCacheModule } from '@nguniversal/common';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { environment } from './../environments/environment';

import { rootRoutes } from './app.routes';

import { CoreModule } from './core/core.module';
import { SharedModule } from './shared/shared.module';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { Ng2Rut } from 'ng2-rut';
import { PagesComponent } from './pages/pages.component';
import { MatIconModule } from '@angular/material/icon';
import { LoadScreenComponent } from './loading/load-screen.component';
import { LoginModule } from './login/login.module';
import { InterceptResponseService } from './response.interceptor/interceptResponse.service';
import { InterceptService } from './response.interceptor/intercept.service';
import { PaginaErrorComponent } from './pagina-error/pagina-error.component';





@NgModule({
  declarations: [
    PaginaErrorComponent,
    AppComponent,
    PagesComponent,
    LoadScreenComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'my-app' }),
    HttpClientModule,
    MatIconModule,
    LoginModule,
    RouterModule.forRoot(rootRoutes, {
      enableTracing: false, // For debugging
      //preloadingStrategy: PreloadAllModules,
     // initialNavigation: 'enabled',
      onSameUrlNavigation: 'reload',
      useHash: true
    }),
    CoreModule,
    SharedModule,
    BrowserAnimationsModule,
    TransferHttpCacheModule,
    Ng2Rut
  ],
  exports: [LoadScreenComponent],
  bootstrap: [AppComponent],
   providers: [
    { provide: LOCALE_ID, useValue: 'es-CL' },
   {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptService,
      multi: true
    },
  {
      provide: HTTP_INTERCEPTORS,
      useClass: InterceptResponseService,
      multi: true
    }
  ]
})
export class AppModule { }
