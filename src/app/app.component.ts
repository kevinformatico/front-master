import { AfterContentInit, Component, ViewChild, ViewEncapsulation, PLATFORM_ID, Inject } from '@angular/core';
import { SideMenuService } from './core';
import { isPlatformBrowser } from '@angular/common';
// tslint:disable-next-line: import-blacklist
import {  Observable } from 'rxjs';
import { LoadScreenService } from './loading/load-screen.service';
import { LoadingInfo } from './loading/load-screen.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./styles/app.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent implements AfterContentInit {
  @ViewChild('drawerContainer') drawerContainer;
  @ViewChild('sideMenu') sideMenu;
  @ViewChild('sideNotifications') sideNotifications;

  loadingStatus: Observable<LoadingInfo>;

  notifications = [];
  messages = [];
  open_menu = false;

  constructor(
    private sideMenuService: SideMenuService,
    private loadingSrv: LoadScreenService,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.loadingStatus = this.loadingSrv.getHttpStatus();
  }


  ngAfterContentInit(): void {
    this.sideMenuService.sidenav = this.sideMenu;
    this.sideMenuService.drawerContainer = this.drawerContainer;
    if (isPlatformBrowser(this.platformId)) {
      this.open_menu = true;
    }
  }
}
