import { Component, OnInit } from '@angular/core';
import { LoadScreenService } from '../loading/load-screen.service';
import { ErrorService } from '../services/error.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-pagina-error',
  templateUrl: './pagina-error.component.html',
  styleUrls: ['./pagina-error.component.scss']
})
export class PaginaErrorComponent implements OnInit {
  readonly ERROR_GENERICO = 'En este momento no es posible atender la consulta del servicio solicitado. Por favor inténtalo más tarde.';

  titulo: string;
  detalle: string;
  display: boolean;
  logout:boolean;

  detalleDefault = this.ERROR_GENERICO;

  constructor(private loadingSrv: LoadScreenService, private errorSrv: ErrorService, private router:Router) {
    errorSrv.detalleError.subscribe(d => {
      this.titulo = d.titulo;
      this.detalle = d.detalle;
      this.logout = d.logout;
    });
  }

  ngOnInit() {
    // Cuando carga la pagina de error se quita el 'loading'
    this.display = true;
    this.loadingSrv.setHttpStatus(false);
  }

  oculta(){
    this.display = false; 
  }

  ocultaLogout(){
    this.display = false; 
    this.router.navigateByUrl('/login');
  }
}
