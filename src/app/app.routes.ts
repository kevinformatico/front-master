import { Routes, RouterModule } from '@angular/router';
import { PagesComponent } from './pages/pages.component';
import { LoginPageComponent } from './login';
import {PaginaErrorComponent} from './pagina-error/pagina-error.component';

export const rootRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule' },
      { path: 'tables', loadChildren: './tables/tables.module#TablesModule' },
      { path: 'error', component: PaginaErrorComponent },
      { path: '', redirectTo: '/login', pathMatch: 'full' }
    ]
  },
  { path: 'login', component: LoginPageComponent }
];

