import { NgModule } from '@angular/core';


import { SearchBarComponent } from './top-navbar/search-bar/search-bar.component';
import { TopNavbarContentComponent } from './top-navbar/content/top-navbar-content.component';

// Side menu stuff
import { CondenseMenuComponent } from './side-menu/condense/condense.component';
import { MenuTopItemsComponent } from './side-menu/top-items/top-items.component';
import { SideMenuContentComponent } from './side-menu/content/side-menu-content.component';
import { SideMenuService } from './side-menu/side-menu.service';
import { AlertaComponent } from '../alerta/alerta.component';



import { ResponsiveBreakpointsComponent } from './responsive-breakpoints/responsive-breakpoints.component';
import { ResponsiveBreakpointDirective } from './responsive-breakpoints/responsive-breakpoint.directive';
import { ResponsiveBreakpointsService } from './responsive-breakpoints/responsive-breakpoints.service';
import { SoloLetrasNumerosDirective } from '../directivas/soloLetrasNumeros.directive';
import { SoloNumerosDirective } from '../directivas/soloNumeros.directive';


// Required modules
import { SharedModule } from '../shared';

@NgModule({
  declarations: [

    AlertaComponent,
    SearchBarComponent,
    TopNavbarContentComponent,
    CondenseMenuComponent,
    MenuTopItemsComponent,
    SideMenuContentComponent,
    SoloLetrasNumerosDirective,
    SoloNumerosDirective,
    ResponsiveBreakpointDirective,
    ResponsiveBreakpointsComponent
  ],
  imports: [
    SharedModule
  ],
  providers: [
    SideMenuService,
    ResponsiveBreakpointsService
  ],
  exports: [
    TopNavbarContentComponent,
    SideMenuContentComponent,
    SoloLetrasNumerosDirective,
    SoloNumerosDirective,
    AlertaComponent,
    ResponsiveBreakpointsComponent
  ]
})
export class CoreModule { }
