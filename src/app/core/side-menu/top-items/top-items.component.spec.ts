import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MenuTopItemsComponent } from './top-items.component';

describe('MenuTopItemsComponent', () => {
    let component: MenuTopItemsComponent;
    let fixture: ComponentFixture<MenuTopItemsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [MenuTopItemsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(MenuTopItemsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});