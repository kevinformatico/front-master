import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CondenseMenuComponent } from './condense.component';

describe('CondenseMenuComponent', () => {
    let component: CondenseMenuComponent;
    let fixture: ComponentFixture<CondenseMenuComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [CondenseMenuComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(CondenseMenuComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});