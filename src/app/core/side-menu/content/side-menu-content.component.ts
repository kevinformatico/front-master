import { Component, ViewEncapsulation } from '@angular/core';
import { TopNavbarContentComponent } from '../../top-navbar/content/top-navbar-content.component';
import { SideMenuService } from '../../side-menu/side-menu.service';
import { ResponsiveBreakpointsService } from '../../responsive-breakpoints/responsive-breakpoints.service';
import { filter } from 'rxjs/operators';
import { AlertComponent } from '../../../shared';
import { MatDialog } from '@angular/material';
import { Router } from '@angular/router';
import { LoginService } from '../../../services/login.service';
import { ContextoService } from '../../../services/contexto.service';
import { ErrorService } from '../../../services/error.service';

@Component({
  selector: 'app-side-menu-content',
  styleUrls: [
    './styles/side-menu-content.scss'
  ],
  templateUrl: './side-menu-content.component.html',
  encapsulation: ViewEncapsulation.None
})

export class SideMenuContentComponent {
  userData: any;
  verAlertaEvaluacion: boolean = false;
  tituloAlerta: string = '';
  detalleAlerta: string = '';
  sideMenuVisible = true;
  constructor(private dialog: MatDialog,
    private responsiveService: ResponsiveBreakpointsService,
    private sideMenuService: SideMenuService,
    private errSrv: ErrorService,
    private _router: Router, private loginSrv: LoginService, private cxtServ: ContextoService) {
    this.userData = this.cxtServ.login;
  }

  logout(): void {
    this.tituloAlerta = '¿Desea salir de la aplicación?';
    this.detalleAlerta = '';
    this.verAlertaEvaluacion = true;
  }

  out() {
    this.loginSrv.logOut().subscribe(result => {
     // if (result) {
        if (result.METADATA.STATUS === '0') {
          this.verAlertaEvaluacion = false;
          this._router.navigate(['/']);
        }else{
          this.verAlertaEvaluacion = false;
        }
     // }


    }, err =>{
      this.verAlertaEvaluacion = false;
      this.errSrv.irLogin('Sesión Expirada', 'Tú sesión a expirado');
    });

  }

  ocultarAlerta() {
    this.verAlertaEvaluacion = false;
  }

  toggleSideMenu(): void {
    this.sideMenuService.sidenav.toggle().then(
      val => {
        this.sideMenuVisible = !this.sideMenuVisible;
      }
    );
  }
}
