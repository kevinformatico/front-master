import { Component, Input, ViewEncapsulation, Inject } from '@angular/core';
import { SideMenuService } from '../../side-menu/side-menu.service';
import { ResponsiveBreakpointsService } from '../../responsive-breakpoints/responsive-breakpoints.service';
import { filter } from 'rxjs/operators';
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material';
import { LoginService } from '../../../services/login.service';
import { ErrorService } from '../../../services/error.service';
import {ContextoService} from '../../../services/contexto.service';

@Component({
  selector: 'app-top-navbar-content',
  styleUrls: ['./styles/top-navbar-content.scss'],
  templateUrl: './top-navbar-content.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TopNavbarContentComponent {
  @Input() messages = [];
  @Input() notifications = [];

  sideMenuVisible = true;
  baseUrl = '';
  verAlertaEvaluacion: boolean = false;
  tituloAlerta: string = '';
  detalleAlerta: string = '';
  nameUser: string = '';

  constructor(
    private sideMenuService: SideMenuService,
    private loginSrv: LoginService,
    private errSrv: ErrorService,
    private ctxService: ContextoService,
    private responsiveService: ResponsiveBreakpointsService,
    private _router: Router,
    private dialog: MatDialog
  ) {
    this.nameUser = this.ctxService.login.nameUser +' '+this.ctxService.login.lastName;
    responsiveService.responsiveSubject
      .pipe(
        filter(breakpoint => breakpoint.screen === 'xs-or-sm')
      )
      .subscribe(breakpoint => {
        if (breakpoint.active) {
          this.sideMenuService.sidenav.mode = 'push';
          this.sideMenuService.sidenav.close().then(
            val => {
              this.sideMenuVisible = false;
            }
          );
        } else {
          this.sideMenuService.sidenav.mode = 'side';
        }
      });

  }

  toggleSideMenu(): void {
    this.sideMenuService.sidenav.toggle().then(
      val => {
        this.sideMenuVisible = !this.sideMenuVisible;
      }
    );
  }

  logout(): void {
    this.tituloAlerta = '¿Desea salir de la aplicación?';
    this.detalleAlerta = '';
    this.verAlertaEvaluacion = true;
  }
  out() {
    this.loginSrv.logOut().subscribe(result => {
        if (result.METADATA.STATUS === '0') {
          this.verAlertaEvaluacion = false;
          this._router.navigate(['/']);
        }else{
          this.verAlertaEvaluacion = false;
        }


    }, err =>{
      this.verAlertaEvaluacion = false;
      this.errSrv.irLogin('Sesión Expirada', 'Tú sesión a expirado');
    });

  }

  ocultarAlerta() {
    this.verAlertaEvaluacion = false;
  }


}
