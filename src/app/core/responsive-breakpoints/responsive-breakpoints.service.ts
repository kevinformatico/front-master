import { Injectable } from '@angular/core';
// tslint:disable-next-line: import-blacklist
import { Subject } from 'rxjs/Rx';

@Injectable()
export class ResponsiveBreakpointsService {
  responsiveSubject: Subject<any> = new Subject();

  constructor() { }
}
