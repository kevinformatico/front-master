import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsiveBreakpointsComponent } from './responsive-breakpoints.component';

describe('ResponsiveBreakpointsComponent', () => {
    let component: ResponsiveBreakpointsComponent;
    let fixture: ComponentFixture<ResponsiveBreakpointsComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ResponsiveBreakpointsComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ResponsiveBreakpointsComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});