import { Component, OnInit, Input, Output, EventEmitter, OnChanges, SimpleChanges, SimpleChange } from '@angular/core';

export const LABELS_ALERTA_DEFAULT1 = {
  titulo: 'Lo sentimos, no fue posible realizar la transacción a través de Santander One Fx.',
  detalle: 'Para mayor información comunícate al 600 320 3000.'
};

@Component({
  selector: 'app-alerta',
  templateUrl: './alerta.component.html',
  styleUrls: ['./alerta.component.scss']
})
export class AlertaComponent implements OnInit {


  tipoError = 'error';
  tipoConfirmacion = 'conf';
  tipoExito = 'exito';

  @Input() display = false;

  icono: string;
  modalIcon: string;
  typebtnE: boolean = false;
  typebtnC: boolean = false;

  @Input() tipoAlerta: string;
  @Input() titulo: string;
  @Input() detalle: string;
  @Input() detalle2: string;
  @Input() nombreAccion1: string;
  @Input() nombreAccion2: string;
  @Input() numbtn: string;
  @Input() nameClass?: string;

  @Output() clickAccion1: EventEmitter<any> = new EventEmitter();
  @Output() clickAccion2: EventEmitter<any> = new EventEmitter();
  @Output() accionCerrar: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
    if (this.numbtn === '1') {
      this.typebtnC = true;
    }
    else {
      this.typebtnE = true;
    }

    if (this.isError()) {
      this.modalIcon = 'modal-icon-alert';
      this.icono = 'glyphicon glyphicon-minus';
    } else if (this.isConfirmacion()) {
      this.modalIcon = 'modal-icon-warning';
      this.icono = 'icon-Info';

    }  else if (this.isExito()) {
      this.modalIcon = 'modal-icon-success';
      this.icono = 'icon-success';
    }
    else {
      throw new Error('Tipo de Alerta no soportado');
    }
  }

  emitirAccion1() {
    this.clickAccion1.emit();
  }

  emitirAccion2() {
    this.clickAccion2.emit();
  }

  emitirAccionCerrar() {
    this.accionCerrar.emit();
  }

  // hayAccion1 verifica que el cliente haya configurado un callback.
  // En caso que se haya configurado, el componente muestra el primer boton.
  hayAccion1() {
    return this.clickAccion1.observers.length > 0;
  }

  // hayAccion2 verifica que el cliente haya configurado un callback.
  // En caso que se haya configurado, el componente muestra un segundo boton.
  hayAccion2() {
    return this.clickAccion2.observers.length > 0;
  }

  // haAccionCerrar verifica que el cliente haya configurado una accion de cerrar dialogo,
  // en caso que se haya configurado el componente muestra una x para cerrar el dialogo
  // en la parte superior derecha.
  hayAccionCerrar() {
    return this.accionCerrar.observers.length > 0;
  }

  isConfirmacion() {
    return this.tipoAlerta === this.tipoConfirmacion;
  }

  isError() {
    return this.tipoAlerta === this.tipoError;
  }
  isExito() {
    return this.tipoAlerta === this.tipoExito;
  }

  public open() {
    this.display = true;
  }

  public close() {
    this.display = false;
  }
}
