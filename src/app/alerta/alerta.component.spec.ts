import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AlertaComponent } from './alerta.component';

describe('AlertaComponent', () => {
  let component: AlertaComponent;
  let fixture: ComponentFixture<AlertaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [AlertaComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AlertaComponent);
    component = fixture.componentInstance;
    component.tipoAlerta = 'exito';
    fixture.detectChanges();
  });



  it(' Should isError true', () => {
    component.tipoAlerta = 'error';
    component.ngOnInit();
    // component.isError();

    expect(component.tipoAlerta).toEqual('error');
    expect(component).toBeTruthy();
    // expect(component.isError).toEqual(true);

  });


  it(' Should isExito true ', () => {
    component.tipoAlerta = 'exito';
    component.ngOnInit();
   
    expect(component.tipoAlerta).toEqual('exito');
    expect(component).toBeTruthy();
    // expect(component.isExito()).toEqual(true);
  });

  it(' Should isConfirmacion true ', () => {
    component.tipoAlerta = 'conf';
    component.ngOnInit();
    expect(component.tipoAlerta).toEqual('conf');
    expect(component).toBeTruthy();
  });




  // xit('should create', () => {
  //   const tipoAlerta: any = component.tipoAlerta;

  //   tipoAlerta.setValue('error');
  //   expect(component.isError).toBeFalsy();

  //   expect(component).toBeTruthy();
  // });




});