export class RutValidatorLocal {

    static clean(rut: string): string {
        if (rut !== undefined) {
            rut = rut === null ? '' : rut;
            // revisar
            for (let i = 0; i < rut.length;) {
                if (rut.charAt(i) === '0') {
                    rut = rut.substring(1, rut.length);
                    i = 0;
                } else {
                    break;
                }
            }
            return typeof rut === 'string' ? rut.replace(/[^0-9kK]+/g, '').toUpperCase() : '';
        }
        return '';
    }

    static formatService(rut: string): string {
        if (rut === null) { rut = ''; }
        while (rut.length < 11) {
            rut = `0${rut}`;
        }
        return rut;
    }

    static isValid(rut: string): boolean {
        const cuerpo = rut.slice(0, -1);
        let dv: any = rut.slice(-1).toUpperCase();
        const valor: any = cuerpo + '-' + dv;
        if (cuerpo.length < 7) { return false; }
        let suma = 0;
        let multiplo = 2;
        for (let i = 1; i <= cuerpo.length; i++) {
            const index = multiplo * valor.charAt(cuerpo.length - i);
            suma = suma + index;
            if (multiplo < 7) { multiplo = multiplo + 1; } else { multiplo = 2; }
        }
        const dvEsperado = 11 - (suma % 11);
        dv = (dv === 'K') ? 10 : dv;
        dv = (dv === 0) ? 11 : dv;
        if (dvEsperado !== dv) { return false; }
        return true;
    }

    static format(rut: string): string {
        rut = this.clean(rut);
        if (rut.length === 0) {
            return '';
        }
        if (rut.length === 1) {
            return rut;
        }
        let result = `${rut.slice(-4, -1)}-${rut.substr(rut.length - 1)}`;
        for (let i = 4; i < rut.length; i += 3) {
            result = rut.slice(-3 - i, -i) + '.' + result;
        }
        return result;
    }
}
