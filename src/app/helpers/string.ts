export class StringHelper {

    static stringCapitalize(text: string): string {
        return text[0].toUpperCase() + text.substr(1).toLowerCase();
    }

    static replaceDiacriticos(text) {
        return text.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
    }
}
