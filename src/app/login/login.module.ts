import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { LoginPageComponent } from './login.component';
import { SharedModule } from '../shared';
import { RutValidator } from 'ng2-rut';
import { HttpModule } from '@angular/http';
import { CoreModule} from '../core/core.module';


export const LoginRoutes = [
  {
    path: '',
    component: LoginPageComponent
  }
];

@NgModule({
  declarations: [
    LoginPageComponent
  ],
  imports: [
    RouterModule.forChild(LoginRoutes),
    SharedModule,
    CoreModule,
    // tslint:disable-next-line: deprecation
    HttpModule
  ],
  exports: [
    LoginPageComponent
  ],
  providers: [
    RutValidator
  ],
  bootstrap: [LoginPageComponent],
})
export class LoginModule { }
