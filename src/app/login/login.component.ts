import { Component, ViewEncapsulation, Inject, PLATFORM_ID, AfterViewInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { isPlatformBrowser } from '@angular/common';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { RutValidator } from 'ng2-rut';
import { User } from '../interface/user.interface';
import { LoginService } from '../services/login.service';
import { RutValidatorLocal } from '../helpers/rut.validator';
//import { AlertComponent } from '../shared';
import { MatDialog } from '@angular/material';
import { StringHelper } from '../helpers/string';
import { ObjectlValidator } from '../helpers/objet.validator';
import { rutValid } from './rutValid';
import { LoadScreenService } from '../loading/load-screen.service';
import { LoadingInfo } from '../loading/load-screen.component';
import { BehaviorSubject } from 'rxjs';
import { ContextoService } from '../services/contexto.service';
import { Login } from '../services/model/login';


@Component({
  selector: 'app-login-page',
  templateUrl: './login.component.html',
  styleUrls: ['./styles/login.scss'],
  //encapsulation: ViewEncapsulation.None,
  providers: []
})

export class LoginPageComponent {

  verAlertaEvaluacion = false;
  tituloAlerta = '';
  detalleAlerta = '';
  rut = /^d{1,2}\.\d{3}\.\d{3}[\-][0-9kK]{1}$/;
  // readonly detalleAlerta = LABELS_ALERTA_DEFAULT1.detalle;

  UID = 'uid';
  KEY_UID = 0;
  VALUE_UID = 1;
  public formLogin: FormGroup;
  baseUrl = '';
  isBrowser: boolean;
  uidArray: any;
  btnValido: boolean = true;

  // Form validation messages
  public validationMessages = {
    rut: [
      { type: 'required', message: 'El Rut es obligatorio.' },
      { type: 'rutValid', message: 'Rut invalido.' }
    ],
    password: [
      { type: 'required', message: 'La clave es obligatoria.' },
      { type: 'minlength', message: 'Debe escribir al menos 4 caracteres' },
      { type: 'maxlength', message: 'Debe escribir maximo 16 caracteres' }
    ]
  };

  sideMenuVisible = false;
  loadingStatus: BehaviorSubject<LoadingInfo>;

  constructor(
    private _router: Router,
    // @Inject(APP_BASE_HREF) private baseHref: string,
    @Inject(PLATFORM_ID) private platformId: Object,
    fb: FormBuilder,
    private loadingSrv: LoadScreenService,
    // private rutValidator: RutValidator,
    private authService: ContextoService,
    private _loginService: LoginService,
    public dialog: MatDialog
  ) {
    this.loadingStatus = new BehaviorSubject({ status: false });
    this.loadingSrv.setHttpStatus(false);

    this.isBrowser = isPlatformBrowser(platformId);
    // this.baseUrl = baseHref;

    this.formLogin = fb.group({
      rut: ['', [
        Validators.required,
        // Validators.pattern(this.rut),
        // Validators.maxLength(12),
        // Validators.minLength(12),
        rutValid
      ]],
      // rut: new FormControl('', Validators.compose([
      //   Validators.required,
      //   this.rutValid
      // ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(4),
        Validators.maxLength(16)
      ])),
    });
  }

  public onSubmit() {
    //user for app with dataPower
    this.loadingStatus.next({ status: true });
    this.loadingSrv.setHttpStatus(true);
    const user: User = {
      'usuario': RutValidatorLocal.formatService(RutValidatorLocal.clean(this.formLogin.value.rut)),
      'clave': this.formLogin.value.password.trim(),
      canal: '71',
      app: 'FHS'
    };


    this.loginUser(user);
  }

  private loginUser(user: User): void {
    //Service Login for app with dataPower
    this._loginService.loginAuth(user).subscribe(
      result => {

        if (result.METADATA.STATUS === '0') {
          const userDataLocal = result.DATA.Login_Response.OUTPUT.hits.hits[0];
          const userData: Login = {
            rutUsuario: userDataLocal._source.rut,
            Rol: userDataLocal._source.rol,
            Profile: userDataLocal._source.profile,
            nameUser: userDataLocal._source.firstName ,
            lastName: userDataLocal._source.lastname

          };

          this.authService.login = userData;
          this.redirecUser();
          //   }
        } else {
          this.loadingStatus.next({ status: false });
          this.loadingSrv.setHttpStatus(false);
          this.btnValido = true;
          this.tituloAlerta = 'Usuario no autorizado';
          this.mostrarAlerta();
        }
      },
      error => {
        this.loadingStatus.next({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.btnValido = true;
        this.tituloAlerta = 'Alguno de los datos que has ingresado es incorrecto.';
        this.mostrarAlerta();
      }
    );
  }

  mostrarAlerta() {
    this.verAlertaEvaluacion = true;
  }

  ocultarAlerta() {
    this.verAlertaEvaluacion = false;
  }




  // TODO:helper del rut, sacarlo aparte
  public formatRut(rut: string) {
    rut = rut.replace(/\.|\-/g, '');
    if (rut.length < 3) {
      return rut;
    }
    const l = rut.length;
    return this.getFormatRut(rut.substr(0, l - 1)) + '-' + rut.substr(l - 1, l);
  }

  private getFormatRut(str: string): string {
    const l = str.length;
    if (str.length <= 3) {
      return str;
    } else {
      return this.getFormatRut(str.substr(0, l - 3)) + '.' + this.getFormatRut(str.substr(l - 3, l));
    }
  }

  rutClean(value: string) {
    return typeof value === 'string' ? value.replace(/[^0-9kK]+/g, '').toLowerCase() : '';
  }

  formatRut2() {
    this.formLogin.get('rut').setValue(this.formatRut(this.formLogin.get('rut').value));
  }

  cleanFormatRut() {
    this.formLogin.get('rut').setValue(this.rutClean(this.formLogin.get('rut').value));
  }
  // TODO: FIN DEL HELPER


  private redirecUser(): void {
    this._router.navigateByUrl('/dashboard');
  }



  private castName(nameArray: string[]): string {
    let executiveName = '';
    if (!ObjectlValidator.isEmpty(nameArray[2])) {
      executiveName += nameArray[2] + ' ';
    }
    if (!ObjectlValidator.isEmpty(nameArray[3])) {
      executiveName += nameArray[3] + ' ';
    }
    if (!ObjectlValidator.isEmpty(nameArray[0])) {
      executiveName += nameArray[0] + ' ';
    }
    if (!ObjectlValidator.isEmpty(nameArray[1])) {
      executiveName += nameArray[1] + ' ';
    }
    return executiveName.trim();
  }


  private splitString(uidString, paramDivide) {
    this.uidArray = uidString.split(paramDivide);
    return this.uidArray;
  }

  private containWord(word) {
    const contains = this.UID.includes(word);
    if (contains) {
      return true;
    }
    return false;
  }


  private isEmail(search: string): boolean {
    let serchfind: boolean;
    // tslint:disable-next-line:max-line-length
    const regexp = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    serchfind = regexp.test(search);
    return serchfind;
  }

  public onKeyPress(event) {
    let elementChecker: string;
    elementChecker = event.target.value;

    if (this.formLogin.get('rut').value.length > 7) {
      const formatK = new RegExp('^[0-9kK]+$');

      if (!formatK.test(elementChecker) || elementChecker.slice(0, -1).indexOf('k') > 1
        || elementChecker.slice(0, -1).indexOf('K') > 1) {
        this.formLogin.get('rut').setValue(elementChecker.slice(0, -1));
      }
    } else {
      const format = new RegExp('^[0-9]+$');
      if (!format.test(elementChecker)) {
        this.formLogin.get('rut').setValue(elementChecker.slice(0, -1));
      }
    }
  }

}
