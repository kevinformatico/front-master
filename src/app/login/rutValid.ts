import { FormControl, AbstractControl, ValidatorFn } from '@angular/forms';
import { ObjectlValidator } from '../helpers/objet.validator';

export function rutValid(control: FormControl): { [key: string]: boolean } | null {
    if (ObjectlValidator.isEmpty(control.value)) {
        return {
            rutValid: null
        };
    } else if (!isValidRUT(control.value)) {
        return {
            rutValid: true
        };
    }
    return null;
}

function isValidRUT(rut: string): boolean {
    rut = rut.replace(/\.|\-/g, '');

    if (rut.length < 2) {
        return false;
    }

    const temp = calulateDV(rut.substr(0, rut.length - 1), 0);
    const ultDigit = rut.substr(rut.length - 1, rut.length);
    const m = Number(temp) % 11;
    const dvCalc = (11 - m);
    let dv: any = 0;
    if (dvCalc === 11) {
        dv = 0;
    } else if (dvCalc === 10) {
        dv = 'K';
    } else {
        dv = dvCalc;
    }
    return String(dv).toLocaleLowerCase() === ultDigit ? true : false;

}

function calulateDV(rut: string, i: number) {
    const serie = [2, 3, 4, 5, 6, 7];
    const idx = (i + 1) <= 5 ? (i + 1) : 0;
    if (rut.length > 1) {
        const ult = rut.substr(rut.length - 1, rut.length);
        const value = Number(ult) * serie[i];
        const rutCut = rut.substr(0, rut.length - 1);
        return value + calulateDV(rutCut, idx);
    } else {
        return Number(rut) * serie[i];
    }
}
