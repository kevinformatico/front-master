import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginPageComponent } from './login.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClient, HttpHandler } from '@angular/common/http';
import { LoadScreenService } from '../loading/load-screen.service';
import { ContextoService } from '../services/contexto.service';

describe('LoginPageComponent', () => {
    let component: LoginPageComponent;
    let fixture: ComponentFixture<LoginPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [LoginPageComponent],
            imports:[FormsModule, ReactiveFormsModule],
            providers:[ HttpClient,
                HttpHandler,LoadScreenService, ContextoService]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(LoginPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        
        expect(component).toBeTruthy();
    });
});