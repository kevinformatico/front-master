import { Injectable } from '@angular/core';
import { ContextoService } from '../services/contexto.service';
import {
  HttpEvent, 
  HttpInterceptor, 
  HttpHandler, 
  HttpRequest,
  HttpResponse
} from '@angular/common/http'
import { Observable } from 'rxjs';
import { catchError,tap} from 'rxjs/operators';


@Injectable(/*{
  providedIn: 'root'
}*/)
export class InterceptService implements HttpInterceptor {
  

  constructor(private ctxSrv: ContextoService) { }
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const token = this.ctxSrv.token;
    const modified = request.clone({
      setHeaders: {
        'access-token': `${(token ? token : '')}`,
        'Content-Type' : 'application/json'
      }
    });

    return next.handle(modified);
  }

}
