// import { environment } from '../../../environments/environment';

/**
 * Clase que encapsula los errores de los servicios y la accion a ejecutar.
 */
export class ResponseError {

  static readonly COD_ERR_INTERNO = 'ERR01';

  
  static readonly ORIGEN_DATAPOWER = 'D';
  static readonly ORIGEN_ELASTICSERCH = 'E';

  constructor(private codigoError: string, private msjError: string, private origen: string = '') {
  }


  get codigo(): string { return this.codigoError; }
  get mensaje(): string { return this.msjError; }
  get origenError(): string { return this.origen; }
}
