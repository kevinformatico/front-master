import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { ResponseError } from './response-error';
import { ErrorService } from '../services/error.service';
import { ContextoService } from '../services/contexto.service';
import { Router } from '@angular/router';


/**
 * Clase encargada de interceptar las response y obtener desde el header el nuevo token de seguridad.
 */
@Injectable()
export class InterceptResponseService implements HttpInterceptor {

  readonly ERROR_GENERICO = 'En este momento no es posible atender la consulta del servicio solicitado. Por favor inténtalo más tarde.';

  constructor(private errorSrv: ErrorService, private ctxSrv: ContextoService, private router: Router) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request).pipe(
      tap(event => {


        if (event instanceof HttpResponse) {

          const newToken = event.headers.get('access-token');

          if (newToken) {
            this.ctxSrv.token = (newToken.trim().length > 0 ? newToken : this.ctxSrv.token);
          }

  
          const resStatus = event.body.METADATA.STATUS;

          if (resStatus) {
            if (resStatus === '101' || resStatus === '650') {

              this.errorSrv.irLogin('Sesión Expirada', 'Tú sesión a expirado');

            }else{
              return;
            }
        }
        if(resStatus === "600"){
          this.errorSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: event.body.METADATA.DESCRIPCION});
        }

        }


      }, (error) => {
       this.errorSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: this.ERROR_GENERICO});
      })
    );
  }
}


