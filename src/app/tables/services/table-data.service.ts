import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ContextoService } from '../../services/contexto.service';
import { environment } from '../../../environments/environment';
import { PropertiesService } from '../../services/propertiesService';
import { Observable } from 'rxjs';
// tslint:disable
@Injectable()
export class TableDataService {
  private urlSpring: string;
  


  constructor(
    private ctxService:ContextoService,
    private http: HttpClient,
    private propertiesSrv:PropertiesService, 
  ) {
    this.urlSpring = this.propertiesSrv.properties.URI_DATAPOWER+'api/';

    const headers = new HttpHeaders().set('Content-Type', 'application/json');
  }

  //get Solicitante
  getSolicitante(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": {"query":{"bool":{"must":[{"term":{"type":"user"}},{"term":{"profile":"solicitante"}}]}}}}', { headers: headers });
  }

  //get Aprobadores
  getAprobadores(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": {"query":{"bool":{"must":[{"term":{"type": "user"}},{"term":{"profile": "aprobador"}}]}}}}', { headers: headers });
  }

  //listar Usuario
  listUSerTableData(): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": {"query":{"bool":{"must":[{"term":{"type":"user"}}]}}}}', { headers: headers });
  }

  //List App 
  getSolicitudTableData(): Observable<any> {
    const json2 = {
      'query': {
        'match': {
          'type': 'apps'
        }
      }
    };
    const json = JSON.stringify(json2);
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return  this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": '+json+'}', { headers: headers });
}
getTableDataAppSol(): Observable<any> {
    
const json2 = {
  'query': {
    'bool': {
      "must": [
      {
        'term': {
        'type': 'apps'
        }
    },
    {
      'term': {
        'solicitante': this.ctxService.login.rutUsuario.toLowerCase()
    }
    }
  ]
  }
  }
};
const json = JSON.stringify(json2);
const headers = new HttpHeaders().set('Content-Type', 'application/json');
return  this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": '+json+'}', { headers: headers });
}
//List App productiva
getHistorico(): Observable<any> {
  const json2 = {
    'query': {
      'match': {
        'type': 'appproductiva'
      }
    }
  };
  const json = JSON.stringify(json2);
  const headers = new HttpHeaders().set('Content-Type', 'application/json');
  return  this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": '+json+'}', { headers: headers });
}


}
