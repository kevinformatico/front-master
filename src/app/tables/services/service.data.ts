import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, from } from 'rxjs';
import { environment } from '../../../environments/environment';
import { ContextoService } from '../../services/contexto.service';
import { PropertiesService } from '../../services/propertiesService';


@Injectable({
    providedIn: 'root'
})
export class ServiceData {

    constructor(  private ctxService:ContextoService,private propertiesSrv:PropertiesService,
        private http: HttpClient){
        this.urlSpring = this.propertiesSrv.properties.URI_DATAPOWER+'api/';
    }
//urlSpring = environment.ACCOUNTS_LIST_URL;
    private urlSpring: string;

getDataSearchVersionP(data): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');   
     return this.http.post(this.urlSpring+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada": '+data+'}', { headers: headers});
}

updateDataSearchVersionP(id, dataUpdate): Observable<any> {
  const headers = new HttpHeaders().set('Content-Type', 'application/json');
  return this.http.post(this.urlSpring + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada":' + dataUpdate + '}', { headers: headers });

}

}