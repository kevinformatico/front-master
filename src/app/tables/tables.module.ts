import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SharedModule } from '../shared';

import { TableDataService } from './services/table-data.service';

import { AdminAppPageComponent } from './pages/adminapp/adminform-app.component';
import { AdminUserPageComponent } from './pages/adminuser/adminform-user.component';
import { ListAppPageComponent } from './pages/listapp/list-app.component';
import { ListUserPageComponent } from './pages/listuser/admin-user.component';
import { ListHistoricoApp } from './pages/historico-app/historico-app.component';
import { FormSolicitudPageComponent } from './pages/formSolicitud/formulario-solicitud.component';

import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatIconModule } from '@angular/material/icon';
import { ConsultUserPageComponent } from './pages/consult-user/consult-user.component';
import { ConsultAppPageComponent } from './pages/consult-app/consult-app.component';
import { CoreModule } from '../core/core.module';
import { UpdateVersionPrComponent } from './pages/update-version-pr/update-version-pr.component';
import { ServiceData } from './services/service.data';


export const tablesRoutes = [
  { path: '', redirectTo: 'adminapp' },
  {
    path: 'adminapp',
    component: AdminAppPageComponent
  },
  { path: '', redirectTo: 'formSolicitud' },
  {
    path: 'formSolicitud',
    component: FormSolicitudPageComponent
  },

  { path: '', redirectTo: 'adminuser' },
  {
    path: 'adminuser',
    component: AdminUserPageComponent
  },
  {
    path: 'consultuser/:id',
    component: ConsultUserPageComponent
  },
  {
    path: 'consultapp/:id',
    component: ConsultAppPageComponent
  },

  { path: '', redirectTo: 'listapp' },
  {
    path: 'listapp',
    component: ListAppPageComponent
  },

  { path: '', redirectTo: 'listuser' },
  {
    path: 'listuser',
    component: ListUserPageComponent
  },

  { path: '', redirectTo: 'listhistorico' },
  {
    path: 'listhistorico',
    component: ListHistoricoApp
  },
  { path: '', redirectTo: 'UpdateVersion' },
  {
    path: 'UpdateVersion',
    component: UpdateVersionPrComponent
  }
];

@NgModule({
  declarations: [
    AdminAppPageComponent,
    AdminUserPageComponent,
    ListAppPageComponent,
    ListUserPageComponent,
    ListHistoricoApp,
    ConsultUserPageComponent,
    ConsultAppPageComponent,
    FormSolicitudPageComponent,
    UpdateVersionPrComponent
  ],

  providers: [
    TableDataService,
    ServiceData
  ],

  imports: [
    RouterModule.forChild(tablesRoutes),
    CoreModule,
    ReactiveFormsModule,
    FormsModule,
    SharedModule,
    CommonModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    MatIconModule

  ]
})
export class TablesModule { }
