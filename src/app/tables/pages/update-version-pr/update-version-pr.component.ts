import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { LoadScreenService } from '../../../loading/load-screen.service';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { Router, ActivatedRoute } from '@angular/router';
import { BehaviorSubject } from 'rxjs';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { ServiceData } from '../../services/service.data';
import { ErrorService } from '../../../services/error.service';
import { Route } from '@angular/compiler/src/core';

@Component({
  selector: 'app-update-version-pr',
  templateUrl: './update-version-pr.html',
  styleUrls: ['./style/update-version-pr.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: []
})
export class UpdateVersionPrComponent implements OnInit {

  loadingStatus: BehaviorSubject<LoadingInfo>;
  btnflag: boolean = false;
  btnflagUp: boolean = false;
  form: FormGroup;
  formUpdate: FormGroup;
  flagDataLoad: boolean = false;
  idDataUpa: any;
  verAlertaExito: boolean = false;
  tituloAlerta1: string = '';
  detalleAlerta1: string = '';
  dataSend: any;
  largoData: number = 0;
  dataResult: any;
  verAlertaErr: boolean = false;
  tituloAlertaErr: string = '';
  detalleAlertaErr: string = '';

  // tslint:disable-next-line: max-line-length
  constructor(private loadingSrv: LoadScreenService, private errSrv: ErrorService, public formBuilder: FormBuilder, private route: Router, private srvData: ServiceData) {
    this.loadingStatus = new BehaviorSubject({ status: false });
    this.form = formBuilder.group({
      codApp: new FormControl('', [Validators.required]),
    });
    this.formUpdate = formBuilder.group({
      Build: new FormControl('', [Validators.required]),
      OS: new FormControl('', [Validators.required]),
      Version: new FormControl('', [Validators.required]),
      Bloquear: new FormControl('', [Validators.required]),
      Mensaje: new FormControl('', [Validators.required]),
      Boton: new FormControl('', [Validators.required])

    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.btnflag = false;
    this.loadingStatus = new BehaviorSubject({ status: true });
    this.loadingSrv.setHttpStatus(true);
    const data = {
      query: {
        bool: {
          filter: [
            {
              term: {
                type: "appproductiva"
              }
            },
            {
              term: {
                CodigoAplicacion: String(this.form.controls['codApp'].value).toLowerCase()

              }
            }
          ]
        }
      }
    };

    const json = JSON.stringify(data);

    this.srvData.getDataSearchVersionP(json).subscribe(result => {
      if (result.DATA.SearchData_Response.INFO.Codigo === '00' && result.DATA.SearchData_Response.OUTPUT.hits.hits.length !== 0) {
        this.largoData = result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data.length - 1;
        this.dataResult = result.DATA.SearchData_Response.OUTPUT.hits.hits;

        this.formUpdate.controls['Version'].setValue(result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data[this.largoData].Version);
        this.formUpdate.controls['OS'].setValue(result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data[this.largoData].OS);
        this.formUpdate.controls['Build'].setValue(result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data[this.largoData].Build);
        this.formUpdate.controls['Mensaje'].setValue(result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data[this.largoData].Mensaje);
        this.formUpdate.controls['Bloquear'].setValue(result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data[this.largoData].Bloquear);
        this.formUpdate.controls['Boton'].setValue(result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data[this.largoData].Boton);
        this.idDataUpa = result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._id;
        this.dataSend = result.DATA.SearchData_Response.OUTPUT.hits.hits[0]._source.data;

        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.flagDataLoad = true;
      } if(result.DATA.SearchData_Response.INFO.Codigo === '00' && result.DATA.SearchData_Response.OUTPUT.hits.total.value === 0) {
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.flagDataLoad = false;
        this.form.controls['codApp'].setValue('');
        this.callAlertaErr();
      }if(result.DATA.SearchData_Response.INFO.Codigo !== '00') {
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.flagDataLoad = false;
        this.errSrv.irPaginaErrorConDetalle({titulo:'Oops, Error',detalle:'Ha ocurrido un error mientras procesamos tu solicitud.'});
      }
    }
    );

  }

  onSubmitUpdate() {
    this.loadingStatus = new BehaviorSubject({ status: true });
    this.loadingSrv.setHttpStatus(true, "Estamos procesando tu solicitud");
    let index = this.dataSend.indexOf(this.dataSend[this.largoData]);

    this.dataSend[index] = this.formUpdate.value;
    const update = {
      doc: {
        data: this.dataSend
      }
    }
    const jsonUpdate = JSON.stringify(update);
    this.srvData.updateDataSearchVersionP(this.idDataUpa, jsonUpdate).subscribe(res => {
      if (res.DATA.UpdateData_Response.INFO.Codigo === "00" || res.DATA.UpdateData_Response.INFO.Codigo === "04") {
     
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.callAlertaExito();
      } else{
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
      }
    }, err => {
      this.loadingStatus = new BehaviorSubject({ status: false });
      this.loadingSrv.setHttpStatus(false);
      this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
    });
  }

  callAlertaExito() {
    this.tituloAlerta1 = 'Exitosa';
    this.detalleAlerta1 = 'La aplicación productiva ha actualizada exitosamente .';
    this.verAlertaExito = true;
  }

  ocultarAlerta1() {
    this.verAlertaExito = false;
    this.route.navigate(["/tables/listhistorico"]);
  }


  callAlertaErr() {
    this.tituloAlertaErr = 'Error';
    this.detalleAlertaErr = 'El dato que buscas no se encuentra.';
    this.verAlertaErr = true;
  }

  ocultarAlertaErr() {
    this.verAlertaErr = false;
  }

}
