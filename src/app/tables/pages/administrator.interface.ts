export interface User {
    rut: string;
    nombre: string;
    celula: string;
    email: string;
}
