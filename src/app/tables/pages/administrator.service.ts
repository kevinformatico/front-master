import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PropertiesService } from '../../services/propertiesService';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { ContextoService } from '../../services/contexto.service';


const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type': 'application/json'
    })
};

@Injectable({
    providedIn: 'root'
})
export class SaveUser {

    private urlAccounts: string;
    private ACCOUNTS_URL: string;

    constructor(public http: HttpClient, private ctxService: ContextoService, private propertiesSrv:PropertiesService) {
        this.urlAccounts = this.propertiesSrv.properties.URI_DATAPOWER+'api/';
    }

    insertUser(usuario): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts+'insertType' , '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": '+JSON.stringify(usuario)+'}', { headers: headers });
    }

    /** Actualiza Usuario */
    updAccountsLocal(id, usuario): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": {"doc":' + JSON.stringify(usuario) + '}}', { headers: headers });
    }

    updAppLocal(id, app): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": {"doc":' + JSON.stringify(app) + '}}', { headers: headers });
    }


    insertApp(app): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'insertType', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": ' + JSON.stringify(app) + '}', { headers: headers });
    }

    existApp(codApp): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + 'search', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada":{ "query":{"bool":{"must":[{"term": {"type": "apps"}},{"term":{"codApp": "' + codApp + '"}}]}}}}', { headers: headers });
    }



    saveSolicitud(data): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        // tslint:disable-next-line: max-line-length
        return this.http.post(this.urlAccounts + 'insertType', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": ' + JSON.stringify(data) + '}', { headers: headers });
    }

    saveSolicitudDP(data): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        // tslint:disable-next-line: max-line-length
        return this.http.post(this.urlAccounts + 'insertType', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": ' + JSON.stringify(data) + '}', { headers: headers });
    }
    //get usuario Solicitante
    getUsuariosSolicitante(): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        // tslint:disable-next-line: max-line-length
        return this.http.post(this.urlAccounts + 'search', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": {"query":{"bool":{"must":[{"term":{"type":"user"}},{"term":{"profile":"solicitante"}}]}}}}', { headers: headers });

    }

      //get usuario Aprobador
      getUsuariosAprobador(): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        // tslint:disable-next-line: max-line-length
        return this.http.post(this.urlAccounts + 'search', '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": {"query":{"bool":{"must":[{"term":{"type":"user"}},{"term":{"profile":"aprobador"}}]}}}}', { headers: headers });

    }



    updApp(id, app): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts + '/upDateByType/' + id, '{ "Cabecera": { "RutUsuario": "' + this.ctxService.login.rutUsuario + '" }, "Entrada": {"doc":' + JSON.stringify(app) + '}}', { headers: headers });
    }


}

