import { Component, Inject, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
//import { APP_BASE_HREF } from '@angular/common';
// tslint:disable-next-line: import-blacklist
import { BehaviorSubject } from 'rxjs';
import { SaveUser } from '../administrator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoadScreenService } from '../../../loading/load-screen.service';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { RutValidatorLocal } from '../../../helpers/rut.validator';
import { ContextoService } from '../../../services/contexto.service';
import { ErrorService } from '../../../services/error.service';
import {TableDataService } from '../../services/table-data.service';

@Component({
    selector: 'app-formulario-solicitud-page',
    templateUrl: './formulario-solicitud.component.html',
    styleUrls: ['./styles/_formulario_solicitud.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: []
})
export class FormSolicitudPageComponent {
    originalData = [];
    dataApps: string[] = [];
    oss: string[] = [];
    aprobadores: any[] = [];
    aprobadoresId: any[] = [];
    allAprobadores: any[] = [];
    dataJson;
    dateSolicitud = new Date();
    userData: any;
    loadingStatus: BehaviorSubject<LoadingInfo>;
    tituloAlerta1 = '';
    detalleAlerta1 = '';
    verAlertaError2: boolean = false;
    verAlertaError:boolean = false;
    btnflag: boolean = true;




    // Full form
    form: FormGroup;
    progress = '0';
    userDataLocal: any;

    constructor(
        private _router: Router,
        public formBuilder: FormBuilder,
        public http: HttpClient,
        private errSrv: ErrorService,
        route: ActivatedRoute,
        private loadingSrv: LoadScreenService,
        private srvTable: TableDataService,
        private ctxService: ContextoService,
        private _saveUSer: SaveUser,
      //  @Inject(APP_BASE_HREF) private baseHref: string
    ) {
        this.userData = this.ctxService.login;


        this.form = formBuilder.group({
            type: new FormControl('', Validators.nullValidator),
            status: new FormControl('', Validators.nullValidator),
            aprobadores: new FormControl('', Validators.nullValidator),
            solicitante: new FormControl('', Validators.required),
            nameApp: new FormControl('', Validators.required),
            canal: new FormControl('', Validators.required),
            version: new FormControl('', Validators.required),
            codApp: new FormControl('', Validators.required),
            os: new FormControl('', Validators.required),
            mensaje: new FormControl('', Validators.required),
            build: new FormControl('', Validators.required),
            id: new FormControl('', Validators.nullValidator),
            fecha: new FormControl('', Validators.nullValidator),
            bloqueo: new FormControl('', [Validators.nullValidator, Validators.required]),
            button: new FormControl('', [Validators.nullValidator, Validators.required])
        });
        this.form.controls['solicitante'].setValue(this.userData.nameUser);
        this.srvTable.getTableDataAppSol().subscribe(res=>{
            if(res.DATA.SearchData_Response.INFO.Codigo === '16'){
              this.errSrv.irPaginaErrorConDetalle({titulo:'Error', detalle:'Ha ocurrido un error mientras procesabamos tu solicitud.'});
            }
            if (res.DATA.SearchData_Response.OUTPUT.hits.hits && res.DATA.SearchData_Response.OUTPUT.hits.hits !== null && res.DATA.SearchData_Response.OUTPUT.hits.hits.length !== 0) {
                this.originalData = res.DATA.SearchData_Response.OUTPUT.hits.hits;
                let i = 0;
        while (i < this.originalData.length) {
            this.dataApps.push(res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source);
            i++;
            }
            }else{
                this.errSrv.irPaginaErrorConDetalle({ titulo: 'Sin Datos', detalle: 'No se encuentran aplicaciones disponibles.' });
            }
        });

 

    }

    validateFields(): void {
        if (!this.form.valid) {
            // Mark the form and inputs as touched so the errors messages are shown
            this.form.markAsTouched();
            for (const control in this.form.controls) {
                if (this.form.controls.hasOwnProperty(control)) {
                    this.form.controls[control].markAsTouched();
                    this.form.controls[control].markAsDirty();
                }
            }
        }
    }

    updateProgress(): void {
        const controls = this.form.controls;
        let size = 0;
        let completed = 0;
        for (const key in controls) {
            if (controls.hasOwnProperty(key)) {
                size++;
                const control = controls[key];
                if ((control.value) && (control.dirty) && (control.valid)) {
                    completed++;
                }
            }
        }

        // Size - 4 to not consider the optional fields
        this.progress = (Math.floor((completed / (size - 1)) * 100)).toString();
    }



    loadData(valueApp) {
        let i = 0;
        while (i < this.dataApps.length) {
            if (this.dataApps[i]['nameApp'] === valueApp) {
                this.form.controls['codApp'].setValue(this.dataApps[i]['codApp']);
                this.form.controls['canal'].setValue(this.dataApps[i]['channel']);
                this.aprobadores = this.dataApps[i]['aprobadores'];
                if (this.dataApps[i]['sisAndroid'] === true) {
                    this.oss.push('Android');
                }
                if (this.dataApps[i]['sisIos'] === true) {
                    this.oss.push('IOS');
                }
            }
            i++;
        }
    }



    mostrarAlerta2() {
        this.verAlertaError2 = true;
    }

    ocultarAlerta2() {
        this.verAlertaError2 = false;
        this._router.navigate(["/dashboard"]);

    }

    onSubmit(): void {
        this.loadingStatus = new BehaviorSubject({ status: true });
        this.loadingSrv.setHttpStatus(true);
        this.btnflag = false;
        this.form.controls['type'].setValue('solicitudes');
        this.form.controls['status'].setValue('ACTIVA');
        const date = new Date;
        const dateNow = date.toLocaleString();
        this.form.controls['fecha'].setValue(dateNow);
        let i = 0;
        while (i < this.aprobadores.length) {
            let aprobador = {
                'rut': this.aprobadores[i].rut,
                'name': this.aprobadores[i].name,
                'status': 'en proceso'
            };
            this.aprobadoresId.push(aprobador);
            i++;
        }
        this.form.controls['aprobadores'].setValue(this.aprobadoresId);
        let rutUserService = RutValidatorLocal.formatService(RutValidatorLocal.clean(this.userData.rutUsuario));
        this.form.controls['id'].setValue(rutUserService);
        const data = this.form.value;
        this._saveUSer.saveSolicitud(data).subscribe(
            result => {
                if (result.DATA.InsertData_Response.INFO.Codigo === "00" && result.DATA.InsertData_Response.OUTPUT.result === 'created') {
                    this.loadingStatus = new BehaviorSubject({ status: false });
                    this.loadingSrv.setHttpStatus(false);
                    this.tituloAlerta1 = 'Resultado Exitoso';
                    this.detalleAlerta1 = 'Se ha registrado con exito';
                    this.mostrarAlerta2();
                } else {
                    if (result.DATA.InsertData_Response.INFO.Codigo !== "00") {
                        this.loadingStatus = new BehaviorSubject({ status: false });
                        this.loadingSrv.setHttpStatus(false);
                        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras realizamos tu solicitud.' });
                    } else {
                        this.errSrv.irPaginaError();
                    }
                }
            },
            error => {
                this.loadingStatus = new BehaviorSubject({ status: false });
                this.loadingSrv.setHttpStatus(false);
                this.tituloAlerta1 = 'Error';

                    this.detalleAlerta1 = 'Ha ocurrido un error al procesar tu solicitud.';
                    this.mostrarAlerta();
                this.btnflag = true;
            }
        );
    }

    mostrarAlerta(){
        this.verAlertaError = true;
    }

    ocultarAlerta1(){
        this.verAlertaError = false;
    }
}
