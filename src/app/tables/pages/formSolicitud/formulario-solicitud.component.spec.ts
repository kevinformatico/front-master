import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormSolicitudPageComponent } from './formulario-solicitud.component';

describe('FormSolicitudPageComponent', () => {
    let component: FormSolicitudPageComponent;
    let fixture: ComponentFixture<FormSolicitudPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [FormSolicitudPageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(FormSolicitudPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});