import { Component, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { TableDataService } from '../../services/table-data.service';
import { ErrorService } from '../../../services/error.service';


export interface RowData {
  nameApp: string;
  codApp: string;
  channel: number;
}
@Component({
  selector: 'app-controls-and-validations-page',
  styleUrls: ['./styles/_historico-app.scss'],
  templateUrl: 'historico-app.component.html',
})
export class ListHistoricoApp {
  dataSource: MatTableDataSource<RowData>;
  @ViewChild(MatSort) sort: MatSort;
  originalData = [];
  listApp = [];
  dataVersion = [];
  visualTable:boolean = false;

  constructor(
    private errSrv: ErrorService,
    private srvApp: TableDataService


  ) {

    this.srvApp.getHistorico().subscribe(res => {
      if (res.DATA.SearchData_Response.INFO.Codigo === '16') {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
      }
      if (res.DATA.SearchData_Response.OUTPUT.hits.hits && res.DATA.SearchData_Response.OUTPUT.hits.hits !== null && res.DATA.SearchData_Response.OUTPUT.hits.hits.length !== 0) {


        this.originalData = res.DATA.SearchData_Response.OUTPUT.hits.hits;

        let i = 0;
        while (i < this.originalData.length) {
          if (res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.type === 'appproductiva') {
            this.listApp.push(res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source);
          }
          i++;
        }

        this.dataSource = new MatTableDataSource(this.listApp);
        if(this.listApp.length !== 0){
          this.visualTable = true;
          this.dataSource.sort = this.sort;
        }
        
      } else {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Sin Datos', detalle: 'No se encontraros datos disponibles.' });
      }
    });

  
  }

  displayedColumns: string[] = ['build', 'version', 'so', 'fecha'];


}

