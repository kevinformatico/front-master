import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListHistoricoApp } from './historico-app.component';

describe('ListHistoricoApp', () => {
    let component: ListHistoricoApp;
    let fixture: ComponentFixture<ListHistoricoApp>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ListHistoricoApp]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListHistoricoApp);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});