import { Component, Inject, ElementRef, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { SaveUser } from '../administrator.service';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { BehaviorSubject } from 'rxjs';
import { LoadScreenService } from '../../../loading/load-screen.service';
import { ErrorService } from '../../../services/error.service';
import { TableDataService } from '../../services/table-data.service';
import {User} from './model/user.model';



@Component({
  selector: 'app-controls-and-validations-page',
  templateUrl: './adminform-app.component.html',
  styleUrls: ['./styles/_regular-tables.scss']
})


export class AdminAppPageComponent {
  MAX_APPLICANTS= 3;
  originalData:any[] = [];
  listUser :any[]= [];
  visible = true;
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA];
  solicitanteCtrl = new FormControl();
  applicantCtrl = new FormControl();
  filteredSolicitante: Observable<string[]>;
  filteredapplicant: Observable<string[]>;
  applicants: any[] = [];
  allApplicants: any[] = [];
  solicitantes: string[] = [];
  allSolicitantes: any[] = [];
  varConfirm:boolean = false;



  @ViewChild('solicitanteInput') solicitanteInput: ElementRef<HTMLInputElement>;
  @ViewChild('applicantInput') applicantInput: ElementRef<HTMLInputElement>;
  @ViewChild('auto') matAutocomplete: MatAutocomplete;
  @ViewChild('autoapp') matAutocompleteapp: MatAutocomplete;


  // Form validation messages
  validationMessages = {
    requiredText: [
      { type: 'required', message: 'This field is required.' }
    ]
  };

  // Full form
  form: FormGroup;
  progress = '0';

  loadingStatus: BehaviorSubject<LoadingInfo>;
  verAlertaError = false;
  tituloAlerta1 = '';
  detalleAlerta1 = '';

  dataAprobador = [];
  dataSolicitante = [];
  aprobadores = [];
  solicitante = [];


  constructor(
    public formBuilder: FormBuilder,
    private http: HttpClient,
    route: ActivatedRoute,
    private loadingSrv: LoadScreenService,
    private _router: Router,
    private errSrv: ErrorService,
    private srvApp: TableDataService,
    private _saveUSer: SaveUser
  ) {
    this.loadingStatus = new BehaviorSubject({ status: false });
    this.loadingSrv.setHttpStatus(false);
    // Defino campos de Formulario
    this.form = formBuilder.group({
      type: new FormControl(),
      nameApp: new FormControl('', Validators.required),
      channel: new FormControl('', Validators.required),
      codApp: new FormControl('', Validators.required),
      aprobadores: new FormControl('', Validators.required),
      sisAndroid: new FormControl(''),
      sisIos: new FormControl(''),
      solicitante: new FormControl('', Validators.required)
    });

    // Cargo data de Usuarios
    this.srvApp.getSolicitante().subscribe(res => {
      if (res.DATA.SearchData_Response.INFO.Codigo === '16') {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
      }
      if (res.DATA.SearchData_Response.OUTPUT.hits.hits && res.DATA.SearchData_Response.OUTPUT.hits.hits !== null && res.DATA.SearchData_Response.OUTPUT.hits.hits.length !== 0) {
       
        this.originalData = res.DATA.SearchData_Response.OUTPUT.hits.hits;
        let i = 0;
        while (i < this.originalData.length) {
          let solicitante: User = {
            rut : res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.rut,
            name : res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.firstName + ' ' + res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.lastname
          }
          this.allSolicitantes.push(solicitante);
          i++
        }
      } else {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Sin Datos', detalle: 'No se encontraros datos disponibles.' });
      }
    });

    this.srvApp.getAprobadores().subscribe(res => {
      if (res.DATA.SearchData_Response.INFO.Codigo === '16') {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
      }
      if (res.DATA.SearchData_Response.OUTPUT.hits.hits && res.DATA.SearchData_Response.OUTPUT.hits.hits !== null && res.DATA.SearchData_Response.OUTPUT.hits.hits.length !== 0) {
       
        this.originalData = res.DATA.SearchData_Response.OUTPUT.hits.hits;
        let i = 0;
        while (i < this.originalData.length) {
          let aprobador :User = {
            rut : res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.rut,
            name: res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.firstName + ' ' + res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.lastname
          }
          this.allApplicants.push(aprobador);
          i++
        }
      } else {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Sin Datos', detalle: 'No se encontraros datos disponibles.' });
      }
    });
  }

  // Function Contact Chips

  add(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;
      if ((value || '').trim()) {
        this.solicitantes.push(value.trim());
        this.form.controls['solicitante'].setValue(this.solicitantes);
      }

      if (input) {
        input.value = '';
        this.form.controls['solicitante'].setValue(this.solicitantes);
      }

      this.solicitanteCtrl.setValue(null);
    }
  }

  addApplicant(event: MatChipInputEvent): void {
    if (!this.matAutocomplete.isOpen) {
      const input = event.input;
      const value = event.value;


      if ((value || '').trim() && this.applicants.length < 3) {
        this.applicants.push(value.trim());
        this.form.controls['aprobadores'].setValue(this.applicants);
      }
      if (input) {
        input.value = '';
        this.form.controls['aprobadores'].setValue(this.applicants);
      }
      this.applicantCtrl.setValue(null);
    }
  }


  remove(solicitante: string): void {
    const index = this.solicitantes.indexOf(solicitante);
    if (index >= 0) {
      this.solicitantes.splice(index, 1);
      this.form.controls['solicitante'].setValue(JSON.stringify(this.solicitantes));
    }
  }

  removeApplicant(applicant: string): void {
    const index = this.applicants.indexOf(applicant);
    const index2 = this.aprobadores.indexOf(aprobadores => aprobadores.nameAprobador == applicant);
    if (index >= 0 ) {
      this.applicants.splice(index, 1);
      this.aprobadores.splice(index2,1)
      this.form.controls['aprobadores'].setValue(this.applicants);
      
    }
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    if (this.solicitantes.length >= 1) {
     
      this.solicitanteInput.nativeElement.value = '';
      this.solicitanteCtrl.setValue(null);
      return;
    }
    this.solicitantes.push(event.option.viewValue);
    this.solicitante.push(event.option.value);
    this.solicitanteInput.nativeElement.value = '';
    this.solicitanteCtrl.setValue(null);
  }

  selectedApplicant(event: MatAutocompleteSelectedEvent): void {
    if (this.applicants.length >= this.MAX_APPLICANTS) {
      this.applicantInput.nativeElement.value = '';
      this.applicantCtrl.setValue(null);
      return;
    } else {
      this.applicants.push(event.option.viewValue);
      this.aprobadores.push(event.option.value);
      this.applicantInput.nativeElement.value = '';
      this.applicantCtrl.setValue(null);
    }

  }


  // Fin Function Contact Chips


  updateProgress(): void {
    const controls = this.form.controls;
    let size = 0;
    let completed = 0;
    for (const key in controls) {
      if (controls.hasOwnProperty(key)) {
        size++;
        const control = controls[key];
        if ((control.dirty) && (control.valid)) {
          completed++;
        }
      }
    }

    this.progress = (Math.floor((completed / (size - 1)) * 100)).toString();
  }

  onSubmit(): void {
    this.loadingStatus = new BehaviorSubject({ status: true });
    this.loadingSrv.setHttpStatus(true);
    if (this.form.controls['sisAndroid'].touched === true || this.form.controls['sisIos'].touched === true) {
      this.form.controls['aprobadores'].setValue(this.aprobadores);
      let validateApp = this.form.controls['codApp'].value.toString();
      this.form.controls['codApp'].setValue(validateApp.toLowerCase());
      this.form.controls['solicitante'].setValue(this.solicitante);
      this.form.controls['type'].setValue('apps');
      const data = this.form.value;
     this._saveUSer.existApp(data.codApp).subscribe(
        result => {
          if (result.DATA.SearchData_Response.INFO.Codigo !== '00' || result.DATA.SearchData_Response.OUTPUT.hits.total.value !== 0) {
            this.loadingStatus = new BehaviorSubject({ status: false });
            this.loadingSrv.setHttpStatus(false);
            this.tituloAlerta1 = 'App Existente';
            this.detalleAlerta1 = 'El código de aplicación que ingresaste ya existe.';
            this.mostrarAlerta1();
          }
          if (result.DATA.SearchData_Response.OUTPUT.hits.total.value === 0) {
            this._saveUSer.insertApp(data).subscribe(
              result => {
                if (result.DATA.InsertData_Response.INFO.Codigo === '00' && result.DATA.InsertData_Response.OUTPUT.result === 'created') {
                  this.loadingStatus = new BehaviorSubject({ status: false });
                  this.loadingSrv.setHttpStatus(false);
                  this.tituloAlerta1 = "Exitosa";
                  this.detalleAlerta1 = 'La aplicación ha sido creada exitosamente';
                  this.varConfirm = true;
                } if (result.DATA.InsertData_Response.INFO.Codigo !== '00') {
                  this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
                }
              }
            );
          }
        }
      );
    }
    else {
      this.loadingStatus = new BehaviorSubject({ status: false });
      this.loadingSrv.setHttpStatus(false);
      this.form.controls['sisAndroid'].setValue(false);
      this.form.controls['sisIos'].setValue(false);
      this.tituloAlerta1 = 'Error Selección';
      this.detalleAlerta1 = 'Debes selecionar a lo menos uno de los Sistemas Operativos';
      this.mostrarAlerta1();
    }


  }

  mostrarAlerta1() {
    this.verAlertaError = true;
  }

  ocultarAlerta1() {
    this.verAlertaError = false;
  }

  onReset(): void {
    this._router.navigate(['/tables/adminapp']);
  }




  ocultarAlerta2() {

    this.varConfirm = false;
    this._router.navigate(["/tables/listapp"]);
  }
}
