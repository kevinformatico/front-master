import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAppPageComponent } from './adminform-app.component';

describe('AdminAppPageComponent', () => {
    let component: AdminAppPageComponent;
    let fixture: ComponentFixture<AdminAppPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [AdminAppPageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(AdminAppPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});