import { Component, Inject, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { RutValidator } from './rut.validator';
// tslint:disable-next-line: import-blacklist
import { SaveUser } from '../administrator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../../services/login.service';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { BehaviorSubject } from 'rxjs';
import { LoadScreenService } from '../../../loading/load-screen.service';
import {ContextoService} from '../../../services/contexto.service';



@Component({
  selector: 'app-controls-and-validations-page',
  templateUrl: './adminform-user.component.html',
  styleUrls: ['./styles/_adminform-user.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: []
})
export class AdminUserPageComponent {

  dateSolicitud = new Date();

  // Full form
  form: FormGroup;
  progress = '0';
  userDataLocal: any;
  verAlertaError = false;
  tituloAlerta1 = '';
  detalleAlerta1 = '';
  verAlertaError2 = false;
  loadingStatus: BehaviorSubject<LoadingInfo>;

  constructor(
    private _router: Router,
    public formBuilder: FormBuilder,
    public http: HttpClient,
    private loadingSrv: LoadScreenService,
    private ctxSrv: ContextoService,
    private _saveUSer: SaveUser,
    private _route: ActivatedRoute
  ) {
    this.loadingStatus = new BehaviorSubject({ status: false });
    this.loadingSrv.setHttpStatus(false);
    this.form = formBuilder.group({
      rut: new FormControl('', Validators.compose([
        Validators.required,
        Validators.maxLength(10),
        Validators.pattern('[0-9]{1,8}[0-9kK]{1}'),
        RutValidator.isValid])),
      type: new FormControl('', Validators.nullValidator),
      firstName: new FormControl('', Validators.required),
      celula: new FormControl('', Validators.required),
      profile: new FormControl('', Validators.required),
      rol: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.email,
        Validators.required
      ])),
      status: new FormControl('', Validators.required)
    });

   

  }

  validateFields(): void {
    if (!this.form.valid) {
      // Mark the form and inputs as touched so the errors messages are shown
      this.form.markAsTouched();
      for (const control in this.form.controls) {
        if (this.form.controls.hasOwnProperty(control)) {
          this.form.controls[control].markAsTouched();
          this.form.controls[control].markAsDirty();
        }
      }
    }
  }

  updateProgress(): void {
    const controls = this.form.controls;
    let size = 0;
    let completed = 0;
    for (const key in controls) {
      if (controls.hasOwnProperty(key)) {
        size++;
        const control = controls[key];
        if ((control.value) && (control.dirty) && (control.valid)) {
          completed++;
        }
      }
    }

    // Size - 4 to not consider the optional fields
    this.progress = (Math.floor((completed / (size - 1)) * 100)).toString();
  }


  onSubmit(): void {
    this.loadingStatus.next({ status: true, titulo: 'Agregando Aplicación.' });
    this.loadingSrv.setHttpStatus(true, 'Agregando Aplicación.');
    this.form.controls['type'].setValue('user');
    const data = this.form.value;
    const rut = '00' + data['rut'];
    data['rut'] = rut;
    this._saveUSer.insertUser(data).subscribe(
      result => {
        if(result.METADATA.STATUS === '0'){
          if (result.DATA.InsertData_Response.OUTPUT.result === 'created') {
            this.loadingStatus = new BehaviorSubject({ status: false });
            this.loadingSrv.setHttpStatus(false);
            this.tituloAlerta1 = 'Resultado Exitoso';
            this.detalleAlerta1 = 'Se a registrado con exito';
            this.mostrarAlerta2();
          }
          if (result.DATA.InsertData_Response.OUTPUT.result === 'updated') {
            this.loadingStatus = new BehaviorSubject({ status: false });
            this.loadingSrv.setHttpStatus(false);
            this.tituloAlerta1 = 'Se a Actualizado';
            this.detalleAlerta1 = 'El usuario que ingresaste ya se encuentra en la Ingresado';
            this.mostrarAlerta1();
          }
        }else{
          this.loadingStatus = new BehaviorSubject({ status: false });
          this.loadingSrv.setHttpStatus(false);
          this.tituloAlerta1 = 'Error';
            this.detalleAlerta1 = 'Ha ocurrido un error';
          this.mostrarAlerta1();
        }
      },
      error => {
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
      }
    );
  }



  mostrarAlerta1() {
    this.verAlertaError = true;
  }

  mostrarAlerta2() {
    this.verAlertaError2 = true;
  }

  ocultarAlerta2() {
    this.verAlertaError2 = false;
    this._router.navigate(["/tables/listuser"]);
  }

  ocultarAlerta1() {

    this.verAlertaError = false;


  }
}
