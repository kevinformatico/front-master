import { FormControl } from '@angular/forms';

export class RutValidator {
  static clean(rut: string): string {
    rut = rut === null ? '' : rut;
    // revisar
    for (let i = 0; i < rut.length;) {
      if (rut.charAt(i) === '0') {
        rut = rut.substring(1, rut.length);
        i = 0;
      } else {
        break;
      }
    }
    return typeof rut === 'string' ? rut.replace(/[^0-9kK]+/g, '').toUpperCase() : '';
  }

  static isValid(fc: FormControl) {
    let rut = fc.value;
    rut = RutValidator.clean(rut);
    let expression = new RegExp('(\\d{6,8})([\\dkK])', 'g');
    if (!expression.test(rut)) {
      return ({ RutValidator: true });
    }
    rut = RutValidator.clean(rut);
    let t = parseInt(rut.slice(0, -1), 10);
    let m = 0;
    let s = 1;
    while (t > 0) {
      s = (s + ((t % 10) * (9 - (m++ % 6)))) % 11;
      t = Math.floor(t / 10);
    }
    const v = (s > 0) ? `${s - 1}` : 'K';
    if (v === rut.slice(-1)) {
      return (null);
    } else {
      return ({ RutValidator: true });
    }
  }

  static formatService(rut: string): string {
    if (rut === null) rut = '';
    while (rut.length < 11) {
      rut = `0${rut}`;
    }
    return rut;
  }

  static format(rut: string): string {
    rut = this.clean(rut);
    if (rut.length === 0) {
      return '';
    }
    if (rut.length === 1) {
      return rut;
    }
    let result = `${rut.slice(-4, -1)}-${rut.substr(rut.length - 1)}`;
    for (let i = 4; i < rut.length; i += 3) {
      result = rut.slice(-3 - i, -i) + '.' + result;
    }
    return result;
  }
}