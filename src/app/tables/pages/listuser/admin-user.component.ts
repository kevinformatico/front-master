import { Component, ViewChild, ViewEncapsulation, Inject, PLATFORM_ID, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatePipe, isPlatformBrowser, NgForOf } from '@angular/common';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { ListUser } from './listuser.service';
import { SaveUser } from '../administrator.service';
import { LoadScreenService } from '../../../loading/load-screen.service';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { Observable, BehaviorSubject } from 'rxjs';
import {TableDataService } from '../../services/table-data.service';
import {ErrorService} from '../../../services/error.service';

const COMMA = 188;
const ENTER = 13;

export interface RowData {
  rut: string;
  nombre: string;
  perfil: string;
  rol: string;
  celula: string;
  email: string;
  status: string;
}

@Component({
  selector: 'app-dashboard-page',
  templateUrl: './admin-user.component.html',
  styleUrls: [
    './styles/_admin-user.scss',
    './layout/pages/lists/styles/lists.scss',
  ],
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})

export class ListUserPageComponent implements OnInit {
  public displayedColumns: string[] = ['select', 'rut', 'nombre', 'profile', 'rol', 'celula', 'email', 'status'];
  dataSource: MatTableDataSource<RowData>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('paginatorTable2') paginatorTable2: MatPaginator;
  isBrowser: boolean;
  dataSourceTable2: MatTableDataSource<RowData>;
  dataSourceT: MatTableDataSource<RowData>;
  selection = new SelectionModel<RowData>(true, []);
  loadingStatus: BehaviorSubject<LoadingInfo>;

  applyFilter(filterValue: string) {
    this.dataSourceTable2.filter = filterValue.trim().toLowerCase();
  }

  // Data from the resolver
  // tslint:disable: member-ordering
  originalData:any[] = [];
  listUser:any[] = [];
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes = [COMMA, ENTER];
  interests = [];

  // Filters for the smart table
  // tslint:disable member-ordering
  filtersForm: FormGroup;
  filtersVisible = true;
  toggleFiltersLabel = 'Hide filters';
  public users: any;
  public user: any;
  verAlertaDesactivar = false;
  tituloAlerta = '';
  detalleAlerta = '';
  numbtn = '';
  verAlertaError = false;
  tituloAlerta1 = '';
  detalleAlerta1 = '';
  visualTable : boolean = false;

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceTable2.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceTable2.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: RowData): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.rut + 1}`;
  }

  ngOnInit() {
    if(this.listUser.length !== 0){
      this.visualTable = true;
      this.dataSourceTable2.sort = this.sort;
      setTimeout(() => this.dataSourceTable2.paginator = this.paginatorTable2);
    }
    
 
  }

  /*
  ngAfterViewInit() {
    this.dataSourceTable2.sort = this.sort;
    setTimeout(() => this.dataSourceTable2.paginator = this.paginatorTable2);
  }*/

  constructor(
    private loadingSrv: LoadScreenService,
    public dialog: MatDialog,
    private srv_admin: SaveUser,
    route: ActivatedRoute,
    private fb: FormBuilder,
    private _listUser: ListUser,
    private errSrv: ErrorService,
    private srvTable: TableDataService,
    private _router: Router,

   // @Inject(APP_BASE_HREF) private baseHref: string,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.loadingStatus = new BehaviorSubject({ status: true });
    this.loadingSrv.setHttpStatus(true);
    this.isBrowser = isPlatformBrowser(platformId);
   this.srvTable.listUSerTableData().subscribe(res =>{
     if(res.METADATA.STATUS !== '0'){
      this.loadingStatus = new BehaviorSubject({ status: false });
      this.loadingSrv.setHttpStatus(false);
      this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
    return;
     }else{
      this.originalData =  res.DATA.SearchData_Response.OUTPUT.hits.hits; 
      if(res.DATA.SearchData_Response.OUTPUT.hits.length !== 0){
        this.visualTable = true;
      }

      let i = 0;
      while (i < this.originalData.length) {
        if (res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.type === 'user') {
          this.listUser.push(res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source);
        }
        i++;
      }
      this.dataSourceTable2 = new MatTableDataSource<RowData>(this.listUser);
      
     }
   
     this.loadingStatus = new BehaviorSubject({ status: false });
     this.loadingSrv.setHttpStatus(false);

    // Set up the form
 

    });
  

    this.filtersForm = fb.group({
      search: '',
      ageRange: [[20, 50]],
      interests: [[]],
      subscribed: ''
    });
 
  }


  public desactivar(selection): void {
    this.ocultarAlerta();
    this.loadingStatus.next({ status: true, titulo: 'Actualizando tu selección.' });
    this.loadingSrv.setHttpStatus(true, 'Actualizando tu selección.');
    for (let s of selection.selected) {
      s['status'] = 'INACTIVO';
      this.srv_admin.updAccountsLocal(s['rut'], s).subscribe(res => {
      });
    }
    this.ocultaLoading();
  }

  ocultaLoading() {
    this.loadingStatus.next({ status: false });
    this.loadingSrv.setHttpStatus(false);
    this.tituloAlerta1 = "Desactición Exitosa";
    this.mostrarAlerta1();

  }

  callDesactivaAler() {
    this.tituloAlerta = "Desactiva Selección";
    this.detalleAlerta = "Estas seguro de Desactivar la selección";
    this.mostrarAlerta();
  }

  mostrarAlerta() {
    this.verAlertaDesactivar = true;
  }

  ocultarAlerta() {
    this.verAlertaDesactivar = false;
  }

  mostrarAlerta1() {

    this.verAlertaError = true;
  }

  ocultarAlerta1() {
    this.verAlertaError = false;
    this._router.navigateByUrl('/RefrshComponent', { skipLocationChange: true }).then(() =>
      this._router.navigate(["/tables/listuser"]));
  }
  //function estatusDesactivo filter status 
  estatusDesactivo(): any {
    return this.dataSourceTable2.data.filter(function (data) {
      return data.status === 'ACTIVO';
    });

  }

}