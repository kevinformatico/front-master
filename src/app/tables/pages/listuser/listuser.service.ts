import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { ContextoService } from '../../../services/contexto.service';
import { environment } from '../../../../environments/environment';
import { PropertiesService } from '../../../services/propertiesService';


@Injectable({
    providedIn: 'root'
})
export class ListUser {

    private urlAccounts: string;
    private ACCOUNTS_URL: string;

    constructor(public http: HttpClient, private ctxService: ContextoService, private propertiesSrv:PropertiesService) {
      this.urlAccounts = this.propertiesSrv.properties.URI_DATAPOWER+'api/';
    }

    getAccountsLocal(): Observable<any> {
        const headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post(this.urlAccounts+'search', '{ "Cabecera": { "RutUsuario": "'+this.ctxService.login.rutUsuario+'" }, "Entrada":{ "query":{"bool":{"must":[{"term":{"type":"apps"}}]}}}}',{ headers: headers });
    }
}
