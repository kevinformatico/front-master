import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListUserPageComponent } from './admin-user.component';

describe('ListUserPageComponent', () => {
    let component: ListUserPageComponent;
    let fixture: ComponentFixture<ListUserPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ListUserPageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListUserPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});