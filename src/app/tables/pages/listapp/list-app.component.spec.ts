import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListAppPageComponent } from './list-app.component';

describe('ListAppPageComponent', () => {
    let component: ListAppPageComponent;
    let fixture: ComponentFixture<ListAppPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ListAppPageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ListAppPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});