import { Component, ViewChild, ViewEncapsulation, Inject, PLATFORM_ID, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DatePipe, isPlatformBrowser } from '@angular/common';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { Router } from '@angular/router';
import { SelectionModel } from '@angular/cdk/collections';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { SaveUser } from '../administrator.service';
import { LoadScreenService } from '../../../loading/load-screen.service';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { Observable, BehaviorSubject } from 'rxjs';
import { ErrorService } from '../../../services/error.service';
import { TableDataService } from '../../services/table-data.service';

const COMMA = 188;
const ENTER = 13;

export interface RowData {
  rut: string;
  nombre: string;
  perfil: string;
  rol: string;
  celula: string;
  email: string;
  status: string;
}

@Component({
  selector: 'app-controls-and-validations-page',
  styleUrls: ['./styles/_list-app.scss'],
  templateUrl: 'list-app.component.html',
  encapsulation: ViewEncapsulation.None,
  providers: [DatePipe]
})

export class ListAppPageComponent implements OnInit {
  public displayedColumns: string[] = ['nombre', 'canal', 'codigo', 'sisOP'];
  dataSource: MatTableDataSource<RowData>;
  @ViewChild(MatSort) sort: MatSort;
  @ViewChild('paginatorTable2') paginatorTable2: MatPaginator;
  isBrowser: boolean;
  dataSourceTable2: MatTableDataSource<RowData>;
  dataSourceT: MatTableDataSource<RowData>;
  selection = new SelectionModel<RowData>(true, []);
  loadingStatus: BehaviorSubject<LoadingInfo>;

  applyFilter(filterValue: string) {
    this.dataSourceTable2.filter = filterValue.trim().toLowerCase();
  }

  // Data from the resolver
  // tslint:disable: member-ordering
  originalData: any[] = [];
  listUser: any[] = [];
  selectable = true;
  removable = true;
  addOnBlur = true;
  separatorKeysCodes = [COMMA, ENTER];
  interests: any[] = [];

  verAlertaDesactivar = false;
  tituloAlerta = '';
  detalleAlerta = '';
  numbtn = '';
  verAlertaError = false;
  tituloAlerta1 = '';
  detalleAlerta1 = '';
  verAlertaExito: boolean = false;
  visualTable: boolean = false;

  // Filters for the smart table
  // tslint:disable member-ordering
  filtersForm: FormGroup;
  filtersVisible = true;
  toggleFiltersLabel = 'Hide filters';
  public users: any;
  public user: any;

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSourceTable2.data.length;
    return numSelected === numRows;
  }

  /** Selects all rows if they are not all selected; otherwise clear selection. */
  masterToggle() {
    this.isAllSelected() ?
      this.selection.clear() :
      this.dataSourceTable2.data.forEach(row => this.selection.select(row));
  }

  /** The label for the checkbox on the passed row */
  checkboxLabel(row?: RowData): string {
    if (!row) {
      return `${this.isAllSelected() ? 'select' : 'deselect'} all`;
    }
    return `${this.selection.isSelected(row) ? 'deselect' : 'select'} row ${row.rut + 1}`;
  }

  ngOnInit() {



  }
  /*
    ngAfterViewInit() {
      this.dataSourceTable2.sort = this.sort;
      setTimeout(() => this.dataSourceTable2.paginator = this.paginatorTable2);
    }*/

  constructor(
    private loadingSrv: LoadScreenService,
    public dialog: MatDialog,
    private srv_admin: SaveUser,
    private errSrv: ErrorService,
    route: ActivatedRoute,
    private fb: FormBuilder,
    private srvApp: TableDataService,
    private _router: Router,
    @Inject(PLATFORM_ID) private platformId: Object
  ) {
    this.loadingStatus = new BehaviorSubject({ status: true });
    this.loadingSrv.setHttpStatus(true);
    this.isBrowser = isPlatformBrowser(platformId);
    this.srvApp.getSolicitudTableData().subscribe(res => {
      if (res.DATA.SearchData_Response.INFO.Codigo === '16') {
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
      }
      if (res.DATA.SearchData_Response.OUTPUT.hits.hits && res.DATA.SearchData_Response.OUTPUT.hits.hits !== null && res.DATA.SearchData_Response.OUTPUT.hits.hits.length !== 0) {
        this.originalData = res.DATA.SearchData_Response.OUTPUT.hits.hits;
        let i = 0;
        while (i < this.originalData.length) {
          if (res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source.type === 'apps') {
            this.listUser.push(res.DATA.SearchData_Response.OUTPUT.hits.hits[i]._source);
          }
          i++;
        }

        this.dataSourceTable2 = new MatTableDataSource(this.listUser);

        this.filtersForm = fb.group({
          search: '',
          ageRange: [[20, 50]],
          interests: [[]],
          subscribed: ''
        });
        if (this.listUser.length !== 0) {
          this.visualTable = true;
          this.dataSourceTable2.sort = this.sort;
          setTimeout(() => this.dataSourceTable2.paginator = this.paginatorTable2);
        }
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
      } else {
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Sin Datos', detalle: 'No se encuentran datos disponibles.' });
      }

    });


  }

  public desactivar(selection): void {
    this.ocultarAlerta();
    this.loadingStatus.next({ status: true, titulo: 'Actualizando tu selección.' });
    this.loadingSrv.setHttpStatus(true, 'Actualizando tu selección.');
    for (let s of selection.selected) {
      s['status'] = 'Desactivo';
      this.srv_admin.updAccountsLocal(s['rut'], s).subscribe(res => {
        if (res.DATA.UpdateData_Response.INFO.Codigo === "00" || res.DATA.UpdateData_Response.INFO.Codigo === "04") {
          this.loadingStatus = new BehaviorSubject({ status: false });
          this.loadingSrv.setHttpStatus(false);
          this.callAlertaExito();
        } if (res.DATA.UpdateData_Response.INFO.Codigo !== "00" || res.DATA.UpdateData_Response.INFO.Codigo !== "04") {
          this.loadingStatus = new BehaviorSubject({ status: false });
          this.loadingSrv.setHttpStatus(false);
          this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
        }
      }, err => {
        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
      });
    }
    this.ocultaLoading();
  }

  ocultaLoading() {
    this.loadingStatus.next({ status: false });
    this.loadingSrv.setHttpStatus(false);
    this.tituloAlerta1 = "Desactivación Exitosa";
    this.mostrarAlerta1();

  }

  callAlertaExito() {
    this.tituloAlerta1 = 'Exito';
    this.detalleAlerta1 = 'Se a realizado con exito la operación que solicitaste.';
    this.verAlertaExito = true;
  }

  ocultaAlertaExito() {
    this.verAlertaExito = false;
  }

  mostrarAlerta1() {
    this.verAlertaError = true;
  }

  ocultarAlerta1() {
    this.verAlertaError = false;
    this._router.navigateByUrl('/RefrshComponent', { skipLocationChange: true }).then(() =>
      this._router.navigate(["/tables/listapp"]));
  }

  //function estatusDesactivo filter status 
  estatusDesactivo(): any {
    return this.dataSourceTable2.data.filter(function (data) {
      return data.status === 'Activo'
    });

  }

  // Nuevo Modal

  callDesactivaAler() {
    this.tituloAlerta = "Desactiva Selección";
    this.detalleAlerta = "Estas seguro de Desactivar la selección";
    this.mostrarAlerta();
  }

  mostrarAlerta() {
    this.verAlertaDesactivar = true;
  }

  ocultarAlerta() {
    this.verAlertaDesactivar = false;
  }

}