import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultUserPageComponent } from './consult-user.component';

describe('ConsultUserPageComponent', () => {
    let component: ConsultUserPageComponent;
    let fixture: ComponentFixture<ConsultUserPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConsultUserPageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConsultUserPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});