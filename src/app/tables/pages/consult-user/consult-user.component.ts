import { Component, Inject, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
//import { APP_BASE_HREF } from '@angular/common';
import { SaveUser } from '../administrator.service';
import { Router, ActivatedRoute } from '@angular/router';
import { LoginService } from '../../../services/login.service';
import { Status } from './status.model';
import { ErrorService } from '../../../services/error.service'


@Component({
  selector: 'consult-user-page',
  templateUrl: './consult-user.component.html',
  styleUrls: ['./styles/_consult-user.scss'],
  encapsulation: ViewEncapsulation.None,
  providers: []
})
export class ConsultUserPageComponent {
  // Full form
  form: FormGroup;
  progress = '0';
  userDataLocal: any;
  idUser: string;
 // statusList = new Array<Status>();

  constructor(
    private _router: Router,
    public formBuilder: FormBuilder,
    public http: HttpClient,
    private errSrv:ErrorService,
    private _route: ActivatedRoute,
    private _loginService: LoginService,
    private _saveUser: SaveUser,
   // @Inject(APP_BASE_HREF) private baseHref: string
  ) {
  
  
/*
    http.get<any>(baseHref + '/assets/data/status.json')
      .subscribe(
        data => {
          data.forEach((a) => {
            this.statusList.push(new Status(a.status));
          });
        }
      );
*/
    this.form = formBuilder.group({
      rut: new FormControl('', Validators.required),
      firstName: new FormControl('', Validators.required),
      celula: new FormControl('', Validators.required),
      profile: new FormControl('', Validators.required),
      rol: new FormControl('', Validators.required),
      email: new FormControl('', Validators.compose([
        Validators.email,
        Validators.required
      ])),
      status: new FormControl('', Validators.required)
    });



  }

  validateFields(): void {
    if (!this.form.valid) {
      // Mark the form and inputs as touched so the errors messages are shown
      this.form.markAsTouched();
      for (const control in this.form.controls) {
        if (this.form.controls.hasOwnProperty(control)) {
          this.form.controls[control].markAsTouched();
          this.form.controls[control].markAsDirty();
        }
      }
    }
  }

  updateProgress(): void {
    const controls = this.form.controls;
    let size = 0;
    let completed = 0;
    for (const key in controls) {
      if (controls.hasOwnProperty(key)) {
        size++;
        const control = controls[key];
        if ((control.value) && (control.dirty) && (control.valid)) {
          completed++;
        }
      }
    }

    // Size - 4 to not consider the optional fields
    this.progress = (Math.floor((completed / (size)) * 100)).toString();
  }



  onSubmit(): void {
    const data = this.form.value;
    this.updUsuario(this.idUser, data);
  }

  updUsuario(id, usuario) {
    this._saveUser.updAccountsLocal(id, usuario).subscribe(
      result => {
        this._router.navigate(['/tables/listuser']);
      },
      error => {
        this.errSrv.irPaginaErrorConDetalle({ titulo: 'Sin Datos', detalle: 'No se encontraros datos disponibles.' });
      }
    );
  }

}
