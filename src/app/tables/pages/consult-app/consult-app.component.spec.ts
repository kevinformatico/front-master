import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultAppPageComponent } from './consult-app.component';

describe('ConsultAppPageComponent', () => {
    let component: ConsultAppPageComponent;
    let fixture: ComponentFixture<ConsultAppPageComponent>;

    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConsultAppPageComponent]
        })
            .compileComponents();
    }));

    beforeEach(() => {
        fixture = TestBed.createComponent(ConsultAppPageComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });

    it('should create', () => {
        expect(component).toBeTruthy();
    });
});