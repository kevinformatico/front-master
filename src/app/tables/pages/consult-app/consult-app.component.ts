import { Component, Inject, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
//import { APP_BASE_HREF } from '@angular/common';
import { COMMA, ENTER } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { MatAutocompleteSelectedEvent, MatChipInputEvent, MatAutocomplete } from '@angular/material';
import { map, startWith } from 'rxjs/operators';
import { LoadingInfo } from '../../../loading/load-screen.component';
import { BehaviorSubject } from 'rxjs';
import { LoadScreenService } from '../../../loading/load-screen.service';
import { SaveUser } from '../administrator.service';
import { ErrorService } from '../../../services/error.service';





@Component({
    selector: 'app-consult-app-page',
    templateUrl: './consult-app.component.html',
    styleUrls: ['./styles/_consult-app.scss'],
    encapsulation: ViewEncapsulation.None,
    providers: []
})
export class ConsultAppPageComponent {
    originalData = [];
    listUser = [];
    visible = true;
    selectable = true;
    removable = true;
    addOnBlur = true;
    separatorKeysCodes: number[] = [ENTER, COMMA];
    solicitanteCtrl = new FormControl();
    applicantCtrl = new FormControl();
    filteredSolicitante: Observable<string[]>;
    filteredapplicant: Observable<string[]>;
    applicants: any[] = [];
    allApplicants: any[] = [];
    solicitantes: string[] = [];
    allSolicitantes: string[] = [];
    userData: [];
    appData: [];
    idApp: string;



    @ViewChild('solicitanteInput') solicitanteInput: ElementRef<HTMLInputElement>;
    @ViewChild('applicantInput') applicantInput: ElementRef<HTMLInputElement>;
    @ViewChild('auto') matAutocomplete: MatAutocomplete;
    @ViewChild('autoapp') matAutocompleteapp: MatAutocomplete;


    // Form validation messages
    validationMessages = {
        requiredText: [
            { type: 'required', message: 'This field is required.' }
        ]
    };

    // Full form
    form: FormGroup;
    progress = '0';

    loadingStatus: BehaviorSubject<LoadingInfo>;
    tituloAlerta1 = '';
    detalleAlerta1 = '';


    dataAprobador = [];
    dataSolicitante = [];
    aprobadores = [];
    varConfirm: boolean = false;



    constructor(
        public formBuilder: FormBuilder,
        private http: HttpClient,
        private errSrv: ErrorService,
        route: ActivatedRoute,
        private loadingSrv: LoadScreenService,
        private _router: Router,
        private _route: ActivatedRoute,
        private _saveUser: SaveUser,
       // @Inject(APP_BASE_HREF) private baseHref: string
    ) {

        const codApp = this._route.snapshot.paramMap.get('id');
        if (null !== codApp) {
            this.getAppData(codApp);
        }

        this.loadingStatus = new BehaviorSubject({ status: false });
        this.loadingSrv.setHttpStatus(false);
        // Defino campos de Formulario
        this.form = formBuilder.group({
            type: new FormControl(),
            nameApp: new FormControl('', Validators.nullValidator),
            channel: new FormControl('', Validators.nullValidator),
            codApp: new FormControl('', Validators.nullValidator),
            aprobadores: new FormControl('', Validators.nullValidator),
            solicitante: new FormControl('', Validators.nullValidator),
            sisAndroid: new FormControl(''),
            sisIos: new FormControl(''),
        });

        this._saveUser.getUsuariosSolicitante().subscribe(
            result => {
                if (result.SearchData_Response.INFO.Codigo === '16') {
                    this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
                }
                this.userData = result.SearchData_Response.OUTPUT.hits.hits;
                let i = 0;
                while (i < this.userData.length) {
                    if (result.SearchData_Response.OUTPUT.hits.hits[i]._source.profile === 'Solicitante') {
                        this.allSolicitantes.push(result.SearchData_Response.OUTPUT.hits.hits[i]._source.firstName);
                    }


                    i++;
                }
            },
            errorLocal => {
                this.errSrv.irPaginaError();
            }
        );

        this._saveUser.getUsuariosAprobador().subscribe(
            result => {
                if (result.SearchData_Response.INFO.Codigo === '16') {
                    this.errSrv.irPaginaErrorConDetalle({ titulo: 'Error', detalle: 'Ha ocurrido un error mientras procesabamos tu solicitud.' });
                }
                this.userData = result.SearchData_Response.OUTPUT.hits.hits;
                let i = 0;
                while (i < this.userData.length) {
                    let aprobador = {
                        'rut': result.SearchData_Response.OUTPUT.hits.hits[i]._source.rut,
                        'nameAprobador': result.SearchData_Response.OUTPUT.hits.hits[i]._source.firstName + ' ' + result.hits.hits[i]._source.lastname
                    }
                    this.allApplicants.push(aprobador);
                    i++;
                }
            });
        this.filteredSolicitante = this.solicitanteCtrl.valueChanges.pipe(
            startWith(null),
            map((solicitante: string | null) => solicitante ? this._filter(solicitante) : this.allSolicitantes.slice()));

        this.filteredapplicant = this.applicantCtrl.valueChanges.pipe(
            startWith(null),
            map((applicant: string | null) => applicant ? this._filteredapplicant(applicant) : this.allApplicants.slice()));

    }

    // Function Contact Chips

    private getAppData(codApp) {
        this._saveUser.existApp(codApp).subscribe(
            result => {
                this.idApp = result.hits.hits[0]._id;
                this.appData = result.hits.hits[0]._source;
                const aprobadoreslength = result.hits.hits[0]._source.aprobadores;
                this.form.controls['nameApp'].setValue(this.appData['nameApp']);
                this.form.controls['channel'].setValue(this.appData['channel']);
                this.form.controls['codApp'].setValue(this.appData['codApp']);
                this.form.controls['sisIos'].setValue(this.appData['sisIos']);
                this.form.controls['sisAndroid'].setValue(this.appData['sisAndroid']);

                this.solicitantes.push(this.appData['solicitante']);
                let i = 0;
                while (i < aprobadoreslength.length) {
                    let aprobador = {
                        'nameAprobador': this.appData['aprobadores'][i]['nameAprobador'],
                        'rut': this.appData['aprobadores'][i]['rut']
                    }
                    this.applicants.push(aprobador.nameAprobador);
                    this.aprobadores.push(aprobador);

                    i++;
                }
            },
            errorLocal => {
                this.errSrv.irPaginaError();
            }
        );
    }

    add(event: MatChipInputEvent): void {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            const value = event.value;
            if ((value || '').trim()) {
                this.solicitantes.push(value.trim());
                this.form.controls['solicitante'].setValue(this.solicitantes);
            }

            if (input) {
                input.value = '';
                this.form.controls['solicitante'].setValue(this.solicitantes);
            }

            this.solicitanteCtrl.setValue(null);
        }
    }

    addApplicant(event: MatChipInputEvent): void {
        if (!this.matAutocomplete.isOpen) {
            const input = event.input;
            const value = event.value;
            if ((value || '').trim()) {
                this.applicants.push(value.trim());
                this.form.controls['aprobadores'].setValue(this.applicants);
            }
            if (input) {
                input.value = '';
                this.form.controls['aprobadores'].setValue(this.applicants);
            }
            this.applicantCtrl.setValue(null);
        }
    }


    remove(solicitante: string): void {
        const index = this.solicitantes.indexOf(solicitante);

        if (index >= 0) {
            this.solicitantes.splice(index, 1);
            this.form.controls['solicitante'].setValue(this.solicitantes);
        }
    }

    removeApplicant(applicant: string): void {
        const index = this.applicants.indexOf(applicant);

        if (index >= 0) {
            this.applicants.splice(index, 1);
            this.form.controls['aprobadores'].setValue(this.applicants);
        }
    }

    selected(event: MatAutocompleteSelectedEvent): void {
        this.solicitantes.push(event.option.viewValue);
        this.solicitanteInput.nativeElement.value = '';
        this.solicitanteCtrl.setValue(null);
    }

    selectedApplicant(event: MatAutocompleteSelectedEvent): void {
        this.applicants.push(event.option.viewValue);
        this.aprobadores.push(event.option.value);
        this.applicantInput.nativeElement.value = '';
        this.applicantCtrl.setValue(null);
    }

    private _filter(value: string): string[] {
        const filterValue = value.toLowerCase();
        return this.allSolicitantes.filter(solicitante => solicitante.toLowerCase().indexOf(filterValue) === 0);
    }

    private _filteredapplicant(value: string): string[] {
        const filterApplicant = value.toLowerCase();
        return this.allApplicants.filter(applicant => applicant.toLowerCase().indexOf(filterApplicant) === 0);
    }

    // Fin Function Contact Chips


    updateProgress(): void {
        const controls = this.form.controls;
        let size = 0;
        let dato = 0;
        let completed = 0;
        for (const key in controls) {
            if (controls.hasOwnProperty(key)) {
                size++;
                const control = controls[key];
                if ((control.dirty) && (control.valid)) {
                    completed++;
                }
            }
        }

        this.progress = (Math.floor((completed / (size - 1)) * 100)).toString();
    }

    onSubmit(): void {
        this.form.controls['aprobadores'].setValue(this.aprobadores);
        this.form.controls['solicitante'].setValue(this.solicitantes);
        this.form.controls['type'].setValue('apps');
        const data = this.form.value;
        this._saveUser.existApp(data.codApp).subscribe(
            result => {
                if (result.SearchData_Response.OUTPUT.hits.hits.length > 0 && this.form.controls['codApp'].value != this.appData['codApp']) {
                    this.loadingStatus = new BehaviorSubject({ status: false });
                    this.loadingSrv.setHttpStatus(false);
                    this.tituloAlerta1 = 'Error Código App';
                    this.detalleAlerta1 = 'Aplicación Existente, Codigo de App ya se encuentra registrado.';
                    this.varConfirm = true;
                } else {
                    this._saveUser.updApp(this.idApp, data).subscribe(
                        res => {
                            if (res.UpdateData_Response.INFO.Codigo === "00" && res.UpdateData_Response.OUTPUT.result === 'updated') {
                                this.loadingStatus = new BehaviorSubject({ status: false });
                                this.loadingSrv.setHttpStatus(false);
                                this.tituloAlerta1 = "Creación Exitosa";
                                this.detalleAlerta1 = '';
                                this.varConfirm = true;
                            } if (res.UpdateData_Response.INFO.Codigo !== "00" || res.UpdateData_Response.INFO.Codigo !== "04") {
                                this.loadingStatus = new BehaviorSubject({ status: false });
                                this.loadingSrv.setHttpStatus(false);
                                this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
                            }
                        },
                        error => {
                            this.loadingStatus = new BehaviorSubject({ status: false });
                            this.loadingSrv.setHttpStatus(false);
                            this.errSrv.irPaginaErrorConDetalle({ titulo: "Oops", detalle: " Ha ocurrido un error mientras se realizaba la operación" });
                        }
                    );
                }
            }
        );
    }


    onReset() {
        this._router.navigate(['tables/listapp'])
    }
    ocultarAlerta2() {

        this.varConfirm = false;
        this._router.navigate(['tables/listapp'])
    }


}
