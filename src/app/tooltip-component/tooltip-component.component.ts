import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { DashboardPageComponent } from '../dashboard/dashboard.component';

@Component({
  selector: 'app-tooltip-component',
  templateUrl: './tooltip-modal.component.html',
  styleUrls: [ './tooltip-modal.component.scss' ]
})
export class TooltipComponentComponent implements OnInit {
  @Input() display:boolean = false;
  @Input() tipoTooltip: string;
  icono: string;
  modalIcon: string;
  tipoConfirmacion = 'conf';
  @Input()data:any;
  @Input()cantAprob:string;



  @Output() clickAccion1: EventEmitter<any> = new EventEmitter();
  @Output() clickAccion2: EventEmitter<any> = new EventEmitter();
  @Output() accionCerrar: EventEmitter<any> = new EventEmitter();
  flag2:boolean = false;
  flag3: boolean = false;
  constructor() { 
    
  }

  ngOnInit() {
 /* if (this.isConfirmacion()) {
      this.modalIcon = 'modal-icon-info ';
      this.icono = 'icon-Info-accounts';
     
    } else {
      throw new Error('Tipo de Alerta no soportado');
    }*/

  }
  public open() {
    this.display = true;
  }

  public close() {
    this.display = false;
  }

   isConfirmacion() {
    return this.tipoTooltip === this.tipoConfirmacion;
  }

  
  protected emitirAccion1() {
    this.clickAccion1.emit();
  }


  protected emitirAccionCerrar() {
    this.accionCerrar.emit();
  }

  // hayAccion1 verifica que el cliente haya configurado un callback.
  // En caso que se haya configurado, el componente muestra el primer boton.
  protected hayAccion1() {
    return this.clickAccion1.observers.length > 0;
  }



  // haAccionCerrar verifica que el cliente haya configurado una accion de cerrar dialogo,
  // en caso que se haya configurado el componente muestra una x para cerrar el dialogo
  // en la parte superior derecha.
  protected hayAccionCerrar() {
    return this.accionCerrar.observers.length > 0;
  }

}
