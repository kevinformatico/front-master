import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
// tslint:disable-next-line: import-blacklist
import { Observable } from 'rxjs';
import { map, catchError, finalize, tap } from 'rxjs/operators';
import { LoadScreenService } from './load-screen.service';

@Injectable()
export class LoadScreenInterceptor implements HttpInterceptor {

  constructor(private status: LoadScreenService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.status.setHttpStatus(true);

    return next.handle(req).pipe(tap((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        this.status.setHttpStatus(false);
      }
    }, (err: any) => {
      this.status.setHttpStatus(false);
    }));
  }

}
