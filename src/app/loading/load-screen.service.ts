import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { LoadingInfo } from './load-screen.component';

@Injectable({
  providedIn: 'root'
})

export class LoadScreenService {
    private showUpLoading: BehaviorSubject<LoadingInfo>;

    constructor() {
        this.showUpLoading = new BehaviorSubject({ status: false });
    }

    setHttpStatus(statusParam: boolean, titulo?: string, detalle?: string) {
        this.showUpLoading.next({
            status: statusParam,
            titulo,
            detalle
        });
    }

    getHttpStatus(): Observable<LoadingInfo> {
        return this.showUpLoading.asObservable();
    }
}
