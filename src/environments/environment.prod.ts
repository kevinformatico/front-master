export const environment = {
  production: true,
  BASE_URL: 'http://localhost:4200', 
  LOGIN_URL: 'https://apiper.santander.cl/ObTablet/login',
  ACCOUNTS_LIST_URL: 'http://localhost:5050/api/',
  URI_DATAPOWER: 'https://180.122.125.150:9580/handshakemanager/'
};
